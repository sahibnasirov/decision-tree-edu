// postcss.config.js

// const postcssPresetEnv = require('postcss-preset-env')
const cssnanoConfig = {
  preset: ['default', { discardComments: { removeAll: true } }]
};

module.exports = {
  plugins: {
    tailwindcss: { config: './tailwind.config.js' },
    autoprefixer: {},
    'postcss-preset-env':{},
    ...(process.env.NODE_ENV === 'production' ? { cssnano: { reset: 'default', } } : {}),
  }

}
//    plugins: [cssnano(), postcssPresetEnv()],