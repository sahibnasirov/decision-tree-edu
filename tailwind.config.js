// tailwind.config.js
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{js,jsx,ts,tsx,vue}'],
  // prefix: 'tw-',
  corePlugins: {
    preflight: false
  }
};