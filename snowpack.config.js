  /** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {

  // alias: {
  //   node_modules: './node_modules',
  // },

  plugins: [

    [
      '@snowpack/plugin-sass',
      {
        compilerOptions: {
          loadPath: ['./node_modules'],
          quiet: true,

        },
      },
    ],

    '@snowpack/plugin-postcss',
    '@snowpack/plugin-svelte',

  ],

  mount: {
    public: {url: '/', static: true},
    src: {url: '/dist'},
  },
  routes: [
    /* Enable an SPA Fallback in development: */
    // {"match": "routes", "src": ".*", "dest": "/index.html"},
  ],
  optimize: {
    bundle: true,
    minify: true,
    treeshake: true,
    target: 'es2018'
  },
  packageOptions: {
  },
  devOptions: {
    tailwindConfig: './tailwind.config.js',
    hmrErrorOverlay: false,
    open: "none"
  },
  buildOptions: {
    /* ... */
  },
};


//webpackConfig
// module: {
//   rules: [
//     {
//       test: /\.css$/,
//       use: ['style-loader', 'css-loader']
//     }

//   ,  {
  //       compilerOptions: {
  //         // style: "expanded",
  //         loadPath: ['./node_modules'],
  //          quietDeps:true
  //       },
  //       },