import { Grid } from "gridjs";
import { select } from 'd3';
// import "/gridjs/dist/theme/mermaid.css";

window.select=select

export function svgTable(){


let grid = new Grid({
  columns: ["Name", "Phone Number", "Email" ],
  data: [
    ["John", "john@example.com", "(353) 01 222 3333"],
    ["Mark", "mark@gmail.com", "(01) 22 888 4444"],
    ["Eoin", "eoin@gmail.com", "0097 22 654 00033"],
    ["Sarah", "sarahcdd@gmail.com", "+322 876 1233"],
    ["Afshin", "afshin@mail.com", "(353) 22 87 8356"]
  ]
}).render(document.getElementById("root"));

}
svgTable()