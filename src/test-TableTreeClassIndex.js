import './global.css'
import { SVGTableClass } from './components/Classes/SVGTableClass';
import { select } from './libs/d3-multi'
let divId = 'root';

let svg =  select('#'+divId).append('svg').attr("width", 800)
  .attr("height", 800)
//.style('background-color', 'lightgrey')

var tableParentEl = svg.append('g').attr('id', 'table1')
 //  .append("foreignObject")
 // // .style("transform", "translate(-80px, 140px)")
 //  .style("width", "200px")
 //  .style("height", "120px")
 //  .append("xhtml:div")
let tableData = {
  "data": {
    "id": 3,
    "parentId": "",
    "name": "split-measure",
    "category": "treeViz",
    "info": " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain",
    "condition": "unknown",
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": "Hot",
            "humidity": "High",
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": "Hot",
            "humidity": "High",
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": "Hot",
            "humidity": "High",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": "Mild",
            "humidity": "High",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": "Cool",
            "humidity": "Normal",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": "Cool",
            "humidity": "Normal",
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": "Cool",
            "humidity": "Normal",
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": "Mild",
            "humidity": "High",
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": "Cool",
            "humidity": "Normal",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": "Mild",
            "humidity": "Normal",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": "Mild",
            "humidity": "Normal",
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": "Mild",
            "humidity": "High",
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": "Hot",
            "humidity": "Normal",
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": "Mild",
            "humidity": "High",
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 5,
            "Yes": 9,
            "group": "all"
          }
        ],
        "entropy": 0.94,
        "maxIgAtrr": "outlook",
        "maxIg": 0.247
      }
    }
  },
  "height": 0,
  "depth": 0,
  "parent": null,
  "id": "3",
  "stepId": 3,
  "category": "treeViz"
}

tableData=tableData.data.tree.data.dataset
window.tableData=tableData
window.select = select
let table = new SVGTableClass({
  element: 'table1',
  data: tableData,
});
window.tabele= table