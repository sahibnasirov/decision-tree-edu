import Handsontable from "handsontable";
import 'handsontable/dist/handsontable.full.css';
import {config, dataProps} from '../config-data/user-config';
import {cloneDeep} from "lodash"

export function   loadTableEditor()   {

  var data = cloneDeep(dataProps.chosenData)
  document.getElementById('modal-content').innerHTML = "";

  var modal = document.getElementById("dataEditDialog");
  modal.style.display = "block";
  modal.style.fontSize = "18px";
  var span = document.getElementById("close");
  span.onclick = function() {
    modal.style.display = "none";
  }

  window.onclick = function(event) {
    if (event.target ===   modal) {
      modal.style.display = "none";
    }
  }


  var container = document.getElementById('modal-content')
  var hot = new Handsontable(container, {
    data: data,
    rowHeaders: true,
    width: "850px",
    height: "600px",
    rowHeights: 40,
    colWidths: 130,
    colHeaders: dataProps.colHeaders,
    minSpareRows: 1,
    minSpareCols: 1,
    allowInsertColumn: true,
    // filters: true,
    // dropdownMenu: true,
    licenseKey: 'non-commercial-and-evaluation'

  });
   hot.refreshDimensions();

   let save = document.getElementById("save")
    save.onclick =function() {
    let dataSet = cloneDeep(hot.getSourceData())
    dataSet = dataSet.filter(el => {return !Object.values(el).some(val => {return val === null })})
    dataProps.chosenData=dataSet
    modal.style.display = "none";

    }


  window.hot = hot
}
