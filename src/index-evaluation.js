import './scss/evaluation-text-field.scss'
import './scss/evaluation-home.scss'
import  './evaluation/evaluation.js'
import './global.css'

// mdc.dataTable.MDCDataTable.attachTo(document.querySelector('.mdc-data-table'));
// mdc.ripple.MDCRipple.attachTo(document.querySelector('.foo-button'));

import * as mdc from 'material-components-web';

import {MDCRipple} from '@material/ripple';
let button = new MDCRipple(document.querySelector('.mdc-button'));

import {MDCTextField} from '@material/textfield';
const textField = new MDCTextField(document.querySelector('.mdc-text-field'));
// new MDCRipple(document.querySelector('.next'));

// import {MDCDialog} from '@material/dialog';
// const dialog = new MDCDialog(document.querySelector('.mdc-dialog'));
//
//
// import {MDCList} from '@material/list';
// const list = new MDCList(document.querySelector('.mdc-dialog .mdc-list'));
//
// dialog.listen('MDCDialog:opened', () => {
//   list.layout();
// });
//
// dialog.listen('MDCDialog:opened', function() {
//   // Assuming contentElement references a common parent element with the rest of the page's content
//   contentElement.setAttribute('aria-hidden', 'true');
// });
// dialog.listen('MDCDialog:closing', function() {
//   contentElement.removeAttribute('aria-hidden');
// });
