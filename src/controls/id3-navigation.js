import { resetSvg } from '../components/id3-tree-renderer';
import {resetId3Config} from '../config-data/id3-config';
import {animatePseudo, resetPseudo} from "../components/animatePseudo";
import {addStepInfo} from '../components/stepUserInfo';
import {barChartTreeVizualizer} from '../components/barChartTreeVisWrapper'
import {tableTreeVizualizer} from '../components/tableTreeVisWrapper'
import {resetWorkspace} from "../config-data/resetWorkspace";
import { select, timeout } from 'd3';
import $ from 'jquery';
import { selectAll } from '../libs/d3-multi';
import {config} from '../config-data/user-config';

var treeVisualizerStyle ;



var stepsList =[]
var maxStep = 0
var abort = false
var stepN = -1
var player
var myTimerSpeed = 2000 // 1 sec

export function resetNavVariables(){
   stepsList =[]
   maxStep = 0
   abort = false
   stepN = -1
   clearInterval(player)
   myTimerSpeed = 2000 // 1 sec


}


function  navigate(stepNumber) {
  
  let step = stepsList[stepNumber]
  if (step.category === 'pseudoCode') {
    addStepInfo(step.info)
    animatePseudo(step.pseudo)
  }
  if (step.category === 'treeViz') {

    treeVisualizerStyle(step)

  }
  console.log("this is log " + stepNumber)
}

function previousStep() {
  $("#play").show()
  $("#pause").hide()
  clearInterval(player);


  if (stepN <= 0) {
    stepN = 0
    navigate(0)
  } else if (stepN > 0) {
    stepN--
    navigate(stepN)
  }
}

function nextStep() {
  $("#play").show()
  $("#pause").hide()
  clearInterval(player);

  if (stepN === maxStep) {
    clearInterval(player);
    selectAll(".node").attr("opacity", 1)

    $("#play").hide()
    $("#pause").hide()
    window.alert("Final step")
  } else if (stepN < maxStep) {
    stepN++
    navigate(stepN)
  }
}

function playforward() {

  if (stepN === maxStep) {
    clearInterval(player);
    selectAll(".node").attr("opacity", 1)
    window.alert("Final step")
  } else if (stepN < maxStep) {
    stepN++
    navigate(stepN)

  }
}


function play() {
  $("#play").hide()
  $("#pause").show()

  player = setInterval(playforward, myTimerSpeed);
}

function pause() {
  $("#play").show()
  $("#pause").hide()
  clearInterval(player);

}

function reset() {
  console.log('reset')
  resetSvg()
  $("#play").show()
  $("#pause").hide()
  clearInterval(player);
  abort = false
  stepN = -1
  clearInterval(player)
  myTimerSpeed = 2000 // 1 sec
  resetPseudo()
  resetId3Config()

  }

export function activateControl(data) {
  if (config.nodeVizTech === 'barChart'){
    resetSvg()
    treeVisualizerStyle = barChartTreeVizualizer;
  }
  else if(config.nodeVizTech === 'tableChart') {
    treeVisualizerStyle = tableTreeVizualizer;
    console.log('table tree visualizer seleted')
  }



  stepsList = data
  // console.log(stepsList)

  maxStep = stepsList.length-2

  select("#play").on('click', play)
  select("#back").on('click', previousStep)
  $("#next").click(nextStep)
  select("#pause").on('click' , pause)
  select("#reset").on('click' , reset)

  //select(".player").insert('div').html('<button id="runTestId3" type="button" title="Reset" ><i class="material-icons">check_circle</i></button>')
  // .on('click', runTestId3)
  play()

}
window.navigate = navigate

