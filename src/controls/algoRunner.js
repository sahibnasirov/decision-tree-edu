import { cloneDeep } from 'lodash';
import { config, dataProps } from '../config-data/user-config';
import { buildId3DecisionTree } from '../algorithms/id3/id3';
import { setNumberofEntry, setTargetClasses } from '../config-data/id3-commonVariables';
import { getTargetClasses } from '../algorithms/id3';
import { activateControl } from './id3-navigation';
import { colorForTargetClasses } from '../algorithms/id3/util/helpers';
import { resetWorkspace } from '../config-data/resetWorkspace';
import { modelList, modelGenerate } from '../config-data/user-config';
import { buildTreeList } from './id3-output-processor';
//test r1d3

import {rid3log} from '../algorithms/r-id3/rid3stepslist';
import {weatherNumeric } from '../algorithms/c4.5/c4.5-sample-data-sets/weather-data-with-numeric';
import {buildId3DecisionTreeRID3 } from '../algorithms/r-id3/r-id3';
window.cloneDeeper=cloneDeep
function runAlgo() {
  resetWorkspace();
  let model = modelGenerate();
  if (config.algo || config.dataset !== null) {

    if (config.algo === 'id3') {
      var sampleDataSet = cloneDeep(dataProps.chosenData);

      var allattrs;
      var nontargetattrs;
      var targetAttributeName;

      // add condition if diferent datasets choosen other than below listed

      if (config.dataset === 'weather') {
        targetAttributeName = 'play';
        allattrs = Object.keys(sampleDataSet[0]);
        nontargetattrs = allattrs.filter(e => e !== targetAttributeName);
        nontargetattrs = nontargetattrs.filter(e => e !== 'day');

      }
      if (config.dataset === 'tic-tac') {
        targetAttributeName = 'classification';
        allattrs = Object.keys(sampleDataSet[0]);
        nontargetattrs = allattrs.filter(e => e !== targetAttributeName);
      }

      if (config.dataset === 'credit') {
        targetAttributeName = 'Buys_computer';
        allattrs = Object.keys(sampleDataSet[0]);
        nontargetattrs = allattrs.filter(e => e !== targetAttributeName);
        nontargetattrs = nontargetattrs.filter(e => e !== 'RID');

      }
      var numberOfEntry = sampleDataSet.length;

      setNumberofEntry(numberOfEntry);
      var targetClasses = getTargetClasses(sampleDataSet, targetAttributeName);
      setTargetClasses(targetClasses);
      colorForTargetClasses();

      const parentId = 0;
      let stepsList = buildId3DecisionTree(sampleDataSet, nontargetattrs, targetAttributeName, parentId);
      window.stepsList = stepsList;

      let listOfTrees = buildTreeList(stepsList);
      let model = modelGenerate();
      model.algo = 'id3';
      model.model = listOfTrees[listOfTrees.length - 1];
      modelList.push(model);

      let pseudoStepsList = stepsList.filter(
        el => el.category !== 'treeViz',
      );

      pseudoStepsList.forEach(el => {
        el.stepId = el.id;
      });
      stepsList = listOfTrees.concat(pseudoStepsList);
      stepsList.sort((a, b) => parseInt(a.stepId) - parseInt(b.stepId));


      window.stepsAll = stepsList;

      window.modelList = modelList;
      activateControl(stepsList);

    }
    if (config.algo === 'c4.5'){
      let sampleDataSet = cloneDeep(weatherNumeric);

      let allattrs;
      let nontargetattrs;
      let targetAttributeName;
      targetAttributeName = 'play';
      allattrs = Object.keys(sampleDataSet[0]);
      nontargetattrs = allattrs.filter(e => e !== targetAttributeName);
      nontargetattrs = nontargetattrs.filter(e => e !== 'day');
      //import c4.5 algorithm
      console.error('c4 5 chosen ')

      let numberOfEntry = sampleDataSet.length;

      setNumberofEntry(numberOfEntry);
      let targetClasses = getTargetClasses(sampleDataSet, targetAttributeName);
      setTargetClasses(targetClasses);
      colorForTargetClasses();

      const parentId = 0;
      console.log(cloneDeep(sampleDataSet))
      let stepsList = buildId3DecisionTreeRID3(sampleDataSet, nontargetattrs, targetAttributeName, parentId)

      window.stepsList = stepsList;

      let listOfTrees = buildTreeList(stepsList);
      let model = modelGenerate();
      model.algo = 'rid3';
      model.model = listOfTrees[listOfTrees.length - 1];
      modelList.push(model);

      let pseudoStepsList = stepsList.filter(
        el => el.category !== 'treeViz',
      );

      pseudoStepsList.forEach(el => {
        el.stepId = el.id;
      });
      stepsList = listOfTrees.concat(pseudoStepsList);
      stepsList.sort((a, b) => parseInt(a.stepId) - parseInt(b.stepId));
      window.stepsAll = stepsList;

      activateControl(stepsList);

    }
  }


}


export { runAlgo };