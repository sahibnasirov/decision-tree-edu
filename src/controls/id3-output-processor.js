import * as d3 from "d3-hierarchy"
import {setFinalNodes} from "../components/customSankeyLink";
import {width, height, setheightSvgResizeD} from "../config-data/id3-config"
import {linksVis, rectanglesVis, render} from "../components/id3-tree-renderer";
import {cloneDeep} from "lodash";
import {barChartYDomain as barChartYDomainFunc} from "../algorithms/id3/util/helpers";
import {setBarChartDomain} from "../config-data/id3-commonVariables"

function processsteps(stepList) {
  let stepsList = stepList
  let listofTrees = []

  let vizStepsList = stepsList.filter(
    el => el.category === "treeViz"
  )
  var cleanstepslist = cloneDeep(vizStepsList)
  let tree = {}

//order by node id
  cleanstepslist.sort((a, b) => parseInt(a.id) - parseInt(b.id));
  cleanstepslist[0].parentId = ""

  let n = cloneDeep(cleanstepslist.length)

  let currentStepList = []
  for (let i = 0; i < n; i++) {

    let newNode = cleanstepslist.shift()
  if ((newNode.type !== "chosen-attr")) {
      currentStepList.push(newNode)

    }
  else if (newNode.type === "chosen-attr") {
      let maxAttr = newNode.name
      currentStepList.forEach(el => {
        if (el.name === maxAttr) {
          el.outcome = "persist"
        }
      })

      let pid = newNode.parentId

      currentStepList = currentStepList.filter(el => {
        return el.parentId !== pid
      })

      let parentIndex = currentStepList.findIndex(el => el.id === pid)
      // console.log(currentStepList)
      if (currentStepList[parentIndex].parentId === "") {
        newNode.parentId = ""
      } else {

        let parentparentID = currentStepList[parentIndex].parentId
        newNode.parentId = parentparentID
      }

      currentStepList = currentStepList.filter(el => {
        return el.id !== pid
      })
      currentStepList.push(newNode)
    }

    currentStepList.sort((a, b) => parseInt(a.id) - parseInt(b.id));
    tree = d3.stratify()(currentStepList)


    tree['stepId'] = newNode.id
    tree['category'] = newNode.category

    listofTrees.push(tree)

  }
  return listofTrees
}
export function buildTreeList(stepsList) {
  let listOfTreeWithposition = []
  let listofTrees = processsteps(stepsList)
  let maxTreeDepth = listofTrees[listofTrees.length-1].height+1


   console.log(cloneDeep(listofTrees))
  listofTrees.forEach( tree => {

    let treeHeight = tree.height+1
    let layoutH =  Math.round((height/3)*treeHeight) - 190
    let layoutW =width
    if(maxTreeDepth>4){
      layoutW = Math.round((width/7)*maxTreeDepth)

    }
    if(layoutH>1000){
       setheightSvgResizeD(layoutH)
    }

    tree  = d3.tree().size([layoutW,layoutH])(tree)

    tree.descendants().forEach(des => {
      try
      {
        let verticalDistance = des.children[0].y -des.y

        if(des.data.type === "split-measure")
        {
          des.children.forEach(ch =>
            {
                ch.y = Math.round(ch.y - verticalDistance +60)
            }
          )
        }
      }
      catch (e) {}

    })


    listOfTreeWithposition.push(tree)
  })
      return listOfTreeWithposition
}
function builedTree(nodeList){
  let tree ={}
  var cleanstepslist = cloneDeep(nodeList)

//order by node id
  cleanstepslist.sort((a, b) => parseInt(a.id) - parseInt(b.id));
  cleanstepslist[0].parentId = ""

  let n = cloneDeep(cleanstepslist.length)

  // let treeD = d3.stratify()(cleanstepslist)
  // let finalTree = layout(treeD)
  //
  //
  // let verticalDistance = finalTree.children[0].y -finalTree.y
  let currentStepList=[]
  for(let i = 0; i<n; i++){

    let newNode = cleanstepslist.shift()
    if((newNode.type !== "chosen-attr")) {
      currentStepList.push(newNode)

    }

    else if(newNode.type === "chosen-attr"){
      let maxAttr = newNode.name
      currentStepList.forEach(el => {if(el.name === maxAttr){ el.outcome = "persist"}})

      let pid =  newNode.parentId

      currentStepList =  currentStepList.filter(el => { return el.parentId !== pid})

      let parentIndex = currentStepList.findIndex(el => el.id === pid)
      // console.log(currentStepList)
      if(currentStepList[parentIndex].parentId === ""){
        newNode.parentId = ""
      }
      else {

        let parentparentID = currentStepList[parentIndex].parentId
        newNode.parentId = parentparentID
      }

      currentStepList =  currentStepList.filter(el => { return el.id !== pid})
      currentStepList.push(newNode)
    }
  }
  currentStepList.sort((a, b) => parseInt(a.id) - parseInt(b.id));
  tree = d3.stratify()(currentStepList)
  let treeHeight = tree.height+1
  let maxtreeHeight = 2 +1
  let curretgeight =  (height/maxtreeHeight)*treeHeight - 190
  tree  = d3.tree().size([width,curretgeight])(tree)

  tree.descendants().forEach(des => {
    try
    {
      let verticalDistance = des.children[0].y -des.y

      if(des.data.type === "split-measure")
      {
        des.children.forEach(ch =>
            {
              if(ch.data.outcome ==="animate")
                ch.y = Math.round(ch.y - verticalDistance +60)
            }
        )
      }
    }
    catch (e) {}

  })
  //console.log(treeslist)
  return tree

}


var nodeList = []
var VizStepN=0

export function resetNodeList() {
  VizStepN=0
  nodeList = []
  let mockstep = {
    "id": -1,
    "parentId": -1
  }
  window.nodeList = nodeList
  nodeList.push(mockstep)
}
resetNodeList()

// /*
// export function   treeVizualizer(step){
//   /!*let curretList = []
//   if(step.id > nodeList[nodeList.length-1].id){
//     nodeList.push(step)
//     nodeList.sort((a, b) => parseInt(a.id) - parseInt(b.id));
//     curretList= nodeList.slice(1,nodeList.length)
//     console.log(curretList)
//     let currentTree = builedTree(curretList)
//     window.currentTree =  currentTree*!/
//
//     let barChartYDomain = barChartYDomainFunc(step)
//     setBarChartDomain(barChartYDomain)
//     setFinalNodes(step)
//
//     linksVis.selectAll("g").remove()
//     rectanglesVis.selectAll("g").remove()
//     render(step)
//   // }
//   // else if(step.id < nodeList[nodeList.length-1].id){
//   //   let index = nodeList.findIndex(item => item.id === step.id);
//   //   curretList = nodeList.slice(1,index)
//   //   nodeList = nodeList.slice(0,index)
//   //
//   //   console.log(curretList)
//   //   let currentTree = builedTree(curretList)
//   //   window.currentTree =  currentTree
//   //
//   //   let barChartYDomain = barChartYDomainFunc(currentTree)
//   //   setBarChartDomain(barChartYDomain)
//   //
//   //   setFinalNodes(currentTree)
//   //
//   //   linksVis.selectAll("g").remove()
//   //   rectanglesVis.selectAll("g").remove()
//   //   render(currentTree)
//   // }
//
// }
// */



