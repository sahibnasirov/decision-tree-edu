  import { filterDatasetByAttrValue, getTargetClasses } from '../../algorithms/id3';
  export function filterData(data, attr){

    let uniqeValuesForMaxIgAttr = getTargetClasses(data, attr);
    let filteredSubsets = [];

    for (let i = 0; i < uniqeValuesForMaxIgAttr.length; i++) {
      let filteredSubset = {};
      filteredSubset['condition'] = uniqeValuesForMaxIgAttr[i];
      filteredSubset['filteredData'] = filterDatasetByAttrValue(
        data,
        attr,
        uniqeValuesForMaxIgAttr[i],
      );
      filteredSubsets.push(filteredSubset);
    }

    return filteredSubsets
  }