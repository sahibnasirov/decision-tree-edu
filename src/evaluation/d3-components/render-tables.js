import {filterData} from './dataFilter';
import {filterTable} from './filter-table';
import {examples,allcolums, features} from '../../algorithms/id3/id3-sample-data-sets/examples';
import {select} from 'd3-selection'

export function renderTables(data, attr, parentDiv) {
  let filtereSubsets = filterData(data, attr)

  filtereSubsets.forEach(subset =>{

   let subTable = filterTable(subset.filteredData, allcolums, features, parentDiv)

  })

}