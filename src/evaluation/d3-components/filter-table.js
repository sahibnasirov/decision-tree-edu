import {select, selectAll, create} from 'd3-selection'
import './basic-table.css'
import { renderTables } from './render-tables';

export function filterTable(data, columns, features, parentDiv) {
  var tablediv = select('#' + parentDiv)
  tablediv.attr('class', 'verticalCon bg-red-500')
  var table = tablediv.append('table')
  var thead = table.append('thead')
  var	tbody = table.append('tbody');

  // append the header row
  thead.append('tr')
    .selectAll('th')
    .data(columns).enter()
    .append('th')
    .text(function (column) { return column; });

  let thList = thead.select('tr')
    .selectAll('th')

    thList.each(function (el) {

      if(features.includes(el)){
        select(this).on('click', () => {
          tablediv.append('div').attr('id', el).attr('class', 'container')
          renderTables(data, el, el)
        })
        console.log(this)
      }

    }

  )



  // create a row for each object in the data
  var rows = tbody.selectAll('tr')
    .data(data)
    .enter()
    .append('tr');

  // create a cell in each row for each column
  var cells = rows.selectAll('td')
    .data(function (row) {
      return columns.map(function (column) {
        return {column: column, value: row[column]};
      });
    })
    .enter()
    .append('td')
    .attr('class', 'bg-cyan-500')
    // .style('background-color', 'black')
    .text(function (d) { return d.value; });

 // return tablediv.node();

}
export function nodeTable(vnode, parentDiv) {
  var data = vnode.tree.data.tree.dataset

  var tablediv = select('#' + parentDiv)
  tablediv.attr('class', 'verticalCon')
  var table = tablediv.append('table')
  var thead = table.append('thead')
  var	tbody = table.append('tbody');
  let columns = Object.keys(data[0])
  // append the header row
  thead.append('tr')
    .selectAll('th')
    .data(columns).enter()
    .append('th')
    .text(function (column) { return column; });


  // create a row for each object in the data
  var rows = tbody.selectAll('tr')
    .data(data)
    .enter()
    .append('tr');

  // create a cell in each row for each column
  var cells = rows.selectAll('td')
    .data(function (row) {
      return columns.map(function (column) {
        return {column: column, value: row[column]};
      });
    })
    .enter()
    .append('td')
    .text(function (d) { return d.value; });

  // return tablediv.node();

}
