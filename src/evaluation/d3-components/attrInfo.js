import {
  entropyOfDataset,
  filterDatasetByAttrValue,
  getValueStatisticsGivenAttr, round,
  uniqeValuesPerAttrs,
} from '../../algorithms/id3';
import { createStep, generateId } from '../../algorithms/id3/stepModel';
import { cloneDeep } from 'lodash';

export function getMaxIgAttr(gdata, gattributes, targetAttributeName, targetClasses, parentId, stepslist, condition) {
  let s_entropy = entropyOfDataset(targetClasses, gdata, targetAttributeName)
  let uniqe_values_per_attrs = uniqeValuesPerAttrs(gdata, gattributes)
  let ig = []
  let maxIg = 0
  let maxIgAttr = ''
  let maxIgAttrStat=[]


  // splitMeasureStep['uniqe_values_per_attrs']=uniqe_values_per_attrs
  splitMeasureStep.tree.data['entropy']=s_entropy
  let spstat = getValueStatisticsGivenAttr(gdata, targetAttributeName, targetClasses)
  spstat['group']='all'
  splitMeasureStep.tree.data.dataset= gdata

  splitMeasureStep.tree.data.stat.push(spstat)

  let newparentId =splitMeasureStep.id



  for (let attr in uniqe_values_per_attrs) {
    let numberOfEntries = gdata.length
    let avgEntropyInformation = 0

    let stat = []
    uniqe_values_per_attrs[attr].forEach(value => {
      let count = 0
      let subset = filterDatasetByAttrValue(gdata, attr, value)

      count = subset.length
      let entropyOfSubset = entropyOfDataset(targetClasses, subset, targetAttributeName)


      let substat = getValueStatisticsGivenAttr(subset,targetAttributeName,targetClasses)
      substat['group'] = value
      substat['Entropy of Subset']=entropyOfSubset
      //substat['subset']=subset

      stat.push(substat)

      //step.tree.id3-sample-data-sets.stat=stat

      avgEntropyInformation += ((count / numberOfEntries) * entropyOfSubset)


    })







    if (ig[attr] > maxIg) {
      maxIg = ig[attr]
      maxIgAttr = attr
      maxIgAttrStat=stat
    }

  }


  // later add sdtepslist after removing children
  let cloneSplitMeasureStep=cloneDeep(splitMeasureStep)

  try {
    delete cloneSplitMeasureStep.children
  }
  catch (e) {

  }
  stepslist.push(cloneSplitMeasureStep)

  let maxIGAtrr
  splitMeasureStep.children.forEach(child => {
    if(child.name === maxIgAttr){
      maxIGAtrr=child
    }
  })

  // maybe just return maxattr
  return maxIGAtrr
}
