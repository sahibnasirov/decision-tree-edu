import React, { useRef, useEffect, useState } from "react";
// import ResizeObserver from "resize-observer-polyfill";


class Table extends Component {
  render() {
    var heading = this.props.heading;
    var body = this.props.body;
    return (
      <table style={{ width: 500 }}>
        <thead>
        <tr>
          {heading.map(head => <th>{head}</th>)}
        </tr>
        </thead>
        <tbody>
        {body.map(row => <TableRow row={row} />)}
        </tbody>
      </table>
    );
  }
}
export function TableRow(row) {
    return (
      <tr>
        {row.map(val => <td>{val}</td>)}
      </tr>
    )
}