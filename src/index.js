import './global.css'
import {select } from "d3"
// import $ from "jquery";
import {loadTableEditor} from "./dataTable/table";
// import {loadTabular} from "./dataTable/importtabular";
import {setAlgo, setDataSet, setNodeVizTech} from "./config-data/user-config";
import {runAlgo} from './controls/algoRunner';
// import * as plot from "./algorithms/id3/entropyPlot.js"
//test imports
import * as d3 from "d3"
window.d3=d3
var dpi = 1/window.devicePixelRatio

if(window.devicePixelRatio>1.1){
   document.querySelector('body').style.zoom = parseFloat(dpi.toString()).toFixed(2)
/*transform: scale(0.5);
transform-origin: top left;*/
}
// document.querySelector('body').style.zoom = `${Math.round((1 / window.devicePixelRatio + Number.EPSILON)*100)/100 * 100}%`;


select("#dropdown-algo-content")
    .insert('div').html('<a href="#" title="Iterative Dichotomiser 3">ID3</a>')
    .on("click", function (){setAlgo('id3')})


select("#dropdown-algo-content")
  .insert('div').html('<a href="#" title="" >C4.5</a>')
  .on("click",  function() {setAlgo('c4.5')})

select("#dropdown-data-set-content")
    .append('div').html('<a href="#">Weather Data</a>')
    .on("click", function (){ setDataSet('weather')})

select("#dropdown-data-set-content")
  .append('div').html('<a href="#">Weather Data (with numeric values)</a>')
  .on("click", function (){ setDataSet('weatherNumeric')})

select("#dropdown-data-set-content")
  .append('div').html('<a href="#">Credit Data</a>')
  .on("click", function (){ setDataSet('credit')})

select("#dropdown-data-set-content")
  .append('div').html('<a href="#"> Tic-Tac-Toe Data</a>')
  .on("click", function (){setDataSet('tic-tac')})



select("#dropdown-style-set-content")
  .append('div').html('<a href="#">Table chart</a>')
  .on("click", function (){ setNodeVizTech('tableChart')})


select("#dropdown-style-set-content")
  .append('div').html('<a href="#">Bar chart</a>')
  .on("click", function (){ setNodeVizTech('barChart')})


 select('#dataEditor').on("click", loadTableEditor)

//select('#dataEditorw3').on("click", loadTabular)
select('#run').on("click", runAlgo)








import { createElement } from 'react'
import { render } from 'react-dom'
import AppSvelte from './components/react/app/AppSvelte.svelte'
import AppReact from './components/react/app/AppReact.jsx'

let appSvelte
let counter = 0
const consoleElement = document.getElementById('console')

function updateCounter(message) {
  counter = counter + 1
  document.getElementById('counter').innerText = `Shared Counter: ${counter}`
  // Update console
  const messageElement = document.createElement('div')
  messageElement.innerHTML = `${message} - ${
    new Date().toTimeString().split(' ')[0]
  }`
  consoleElement.prepend(messageElement)
  // Update React, Svelte, and Vue
  renderReact()
  appSvelte.$set({
    counter,
  })
}

function renderReact() {
  render(
    createElement(AppReact, {
      counter,
      onChange: () => updateCounter('React'),
    }),
    document.getElementById('react-root')
  )
}
function renderSvelte() {
  appSvelte = new AppSvelte({
    target: document.getElementById('svelte-root'),
    props: {
      counter,
      onChange: () => updateCounter('Svelte'),
    },
  })
}

// renderReact()
// renderSvelte()

