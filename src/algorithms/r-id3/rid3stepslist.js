export var rid3log = [
  {
    "id": 1,
    "parentId": null,
    "name": null,
    "info": "Algorithm Starts",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 2,
    "parentId": null,
    "name": null,
    "info": "Function Call 1 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 4,
    "parentId": 3,
    "name": "outlook",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: outlook",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 3,
            "Yes": 2,
            "group": "Sunny",
            "Entropy of Subset": 0.97
          },
          {
            "No": 0,
            "Yes": 4,
            "group": "Overcast",
            "Entropy of Subset": 0
          },
          {
            "No": 2,
            "Yes": 3,
            "group": "Rain",
            "Entropy of Subset": 0.97
          }
        ],
        "entropy": 0.6928571428571428,
        "ig": 0.247
      }
    }
  },
  {
    "id": 5,
    "parentId": 3,
    "name": "temp",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: temp",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 85,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 80,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 83,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 70,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 68,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 65,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 64,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": 72,
            "Entropy of Subset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 69,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 2,
            "group": 75,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 81,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 71,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0.14285714285714285,
        "ig": 0.797
      }
    }
  },
  {
    "id": 6,
    "parentId": 3,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 85,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": 90,
            "Entropy of Subset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 78,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 96,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 2,
            "group": 80,
            "Entropy of Subset": 0.92
          },
          {
            "No": 1,
            "Yes": 2,
            "group": 70,
            "Entropy of Subset": 0.92
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 65,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 95,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 75,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0.5371428571428571,
        "ig": 0.403
      }
    }
  },
  {
    "id": 7,
    "parentId": 3,
    "name": "wind",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: wind",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 2,
            "Yes": 6,
            "group": "Weak",
            "Entropy of Subset": 0.81
          },
          {
            "No": 3,
            "Yes": 3,
            "group": "Strong",
            "Entropy of Subset": 1
          }
        ],
        "entropy": 0.8914285714285715,
        "ig": 0.049
      }
    }
  },
  {
    "id": 8,
    "parentId": 3,
    "name": "temp",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: temp",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": 83,
        "entropy": 0.114,
        "ig": 0.114
      }
    }
  },
  {
    "id": 9,
    "parentId": 3,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": 70,
        "entropy": 0.046,
        "ig": 0.046
      }
    }
  },
  {
    "id": 10,
    "parentId": 3,
    "name": "outlook",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: outlook",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 3,
            "Yes": 2,
            "group": "Sunny",
            "Entropy of Subset": 0.97
          },
          {
            "No": 0,
            "Yes": 4,
            "group": "Overcast",
            "Entropy of Subset": 0
          },
          {
            "No": 2,
            "Yes": 3,
            "group": "Rain",
            "Entropy of Subset": 0.97
          }
        ],
        "entropy": 0.6928571428571428,
        "ig": 0.247
      }
    }
  },
  {
    "id": 11,
    "parentId": 3,
    "name": "temp",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: temp",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 85,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 80,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 83,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 70,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 68,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 65,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 64,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": 72,
            "Entropy of Subset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 69,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 2,
            "group": 75,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 81,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 71,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0.14285714285714285,
        "ig": 0.797
      }
    }
  },
  {
    "id": 12,
    "parentId": 3,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 85,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": 90,
            "Entropy of Subset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 78,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 96,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 2,
            "group": 80,
            "Entropy of Subset": 0.92
          },
          {
            "No": 1,
            "Yes": 2,
            "group": 70,
            "Entropy of Subset": 0.92
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 65,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 95,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 75,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0.5371428571428571,
        "ig": 0.403
      }
    }
  },
  {
    "id": 13,
    "parentId": 3,
    "name": "wind",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: wind",
    "condition": "unknown",
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 2,
            "Yes": 6,
            "group": "Weak",
            "Entropy of Subset": 0.81
          },
          {
            "No": 3,
            "Yes": 3,
            "group": "Strong",
            "Entropy of Subset": 1
          }
        ],
        "entropy": 0.8914285714285715,
        "ig": 0.049
      }
    }
  },
  {
    "id": 3,
    "name": "split-measure",
    "category": "treeViz",
    "info": " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain",
    "condition": "unknown",
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 5,
            "Yes": 9,
            "group": "all"
          }
        ],
        "entropy": 0.94,
        "maxIg": 0.797
      }
    }
  },
  {
    "id": 14,
    "parentId": 3,
    "name": "temp",
    "category": "treeViz",
    "info": "temp is selected with Maximum Information Gain 0.797",
    "condition": "unknown",
    "type": "chosen-attr",
    "outcome": "",
    "pseudo": [
      "line4",
      "line5"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "temp": 85,
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D2",
            "outlook": "Sunny",
            "temp": 80,
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D3",
            "outlook": "Overcast",
            "temp": 83,
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D4",
            "outlook": "Rain",
            "temp": 70,
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D5",
            "outlook": "Rain",
            "temp": 68,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D6",
            "outlook": "Rain",
            "temp": 65,
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          },
          {
            "day": "D7",
            "outlook": "Overcast",
            "temp": 64,
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D8",
            "outlook": "Sunny",
            "temp": 72,
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D9",
            "outlook": "Sunny",
            "temp": 69,
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D10",
            "outlook": "Rain",
            "temp": 75,
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "temp": 75,
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "temp": 72,
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          },
          {
            "day": "D13",
            "outlook": "Overcast",
            "temp": 81,
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D14",
            "outlook": "Rain",
            "temp": 71,
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 85,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 80,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 83,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 70,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 68,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 65,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 64,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": 72,
            "Entropy of Subset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 69,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 2,
            "group": 75,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 81,
            "Entropy of Subset": 0
          },
          {
            "No": 1,
            "Yes": 0,
            "group": 71,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0.14285714285714285,
        "ig": 0.797
      }
    }
  },
  {
    "id": 15,
    "parentId": null,
    "name": null,
    "info": "removed seleted attribute temp from the non target attr list",
    "pseudo": [
      "line6"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 16,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line7"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 17,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 18,
    "parentId": null,
    "name": null,
    "info": "Function Call 2 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 19,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label No",
    "condition": 85,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D1",
            "outlook": "Sunny",
            "humidity": 85,
            "wind": "Weak",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "group": "No"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 20,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 21,
    "parentId": null,
    "name": null,
    "info": "Function Call 3 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 22,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label No",
    "condition": 80,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D2",
            "outlook": "Sunny",
            "humidity": 90,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "group": "No"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 23,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 24,
    "parentId": null,
    "name": null,
    "info": "Function Call 4 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 25,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 83,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D3",
            "outlook": "Overcast",
            "humidity": 78,
            "wind": "Weak",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 26,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 27,
    "parentId": null,
    "name": null,
    "info": "Function Call 5 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 28,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 70,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D4",
            "outlook": "Rain",
            "humidity": 96,
            "wind": "Weak",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 29,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 30,
    "parentId": null,
    "name": null,
    "info": "Function Call 6 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 31,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 68,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D5",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 32,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 33,
    "parentId": null,
    "name": null,
    "info": "Function Call 7 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 34,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label No",
    "condition": 65,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D6",
            "outlook": "Rain",
            "humidity": 70,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "group": "No"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 35,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 36,
    "parentId": null,
    "name": null,
    "info": "Function Call 8 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 37,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 64,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D7",
            "outlook": "Overcast",
            "humidity": 65,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 38,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 39,
    "parentId": null,
    "name": null,
    "info": "Function Call 9 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 41,
    "parentId": 40,
    "name": "outlook",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: outlook",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": "Sunny",
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Overcast",
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 42,
    "parentId": 40,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 95,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 90,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 43,
    "parentId": 40,
    "name": "wind",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: wind",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": "Weak",
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Strong",
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 44,
    "parentId": 40,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": 90,
        "entropy": null,
        "ig": null
      }
    }
  },
  {
    "id": 45,
    "parentId": 40,
    "name": "outlook",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: outlook",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": "Sunny",
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Overcast",
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 46,
    "parentId": 40,
    "name": "humidity",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: humidity",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": 95,
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": 90,
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 47,
    "parentId": 40,
    "name": "wind",
    "category": "treeViz",
    "info": "Calculating Entropy and Information Gain for Attr: wind",
    "condition": 72,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": "Weak",
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Strong",
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 40,
    "parentId": 14,
    "name": "split-measure",
    "category": "treeViz",
    "info": " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain",
    "condition": 72,
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 1,
            "group": "all"
          }
        ],
        "entropy": 1,
        "maxIg": 1
      }
    }
  },
  {
    "id": 48,
    "parentId": 40,
    "name": "outlook",
    "category": "treeViz",
    "info": "outlook is selected with Maximum Information Gain 1",
    "condition": 72,
    "type": "chosen-attr",
    "outcome": "",
    "pseudo": [
      "line4",
      "line5"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "outlook": "Sunny",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          },
          {
            "day": "D12",
            "outlook": "Overcast",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "No": 1,
            "Yes": 0,
            "group": "Sunny",
            "Entropy of Subset": 0
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Overcast",
            "Entropy of Subset": 0
          }
        ],
        "entropy": 0,
        "ig": 1
      }
    }
  },
  {
    "id": 49,
    "parentId": null,
    "name": null,
    "info": "removed seleted attribute outlook from the non target attr list",
    "pseudo": [
      "line6"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 50,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line7"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 51,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 52,
    "parentId": null,
    "name": null,
    "info": "Function Call 10 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 53,
    "parentId": 48,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label No",
    "condition": "Sunny",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D8",
            "humidity": 95,
            "wind": "Weak",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "group": "No"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 54,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 55,
    "parentId": null,
    "name": null,
    "info": "Function Call 11 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 56,
    "parentId": 48,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": "Overcast",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D12",
            "humidity": 90,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 57,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line10"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 58,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 59,
    "parentId": null,
    "name": null,
    "info": "Function Call 12 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 60,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 69,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D9",
            "outlook": "Sunny",
            "humidity": 70,
            "wind": "Weak",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 61,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 62,
    "parentId": null,
    "name": null,
    "info": "Function Call 13 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 63,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 75,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D10",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Weak",
            "play": "Yes"
          },
          {
            "day": "D11",
            "outlook": "Sunny",
            "humidity": 70,
            "wind": "Strong",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 2,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 64,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 65,
    "parentId": null,
    "name": null,
    "info": "Function Call 14 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 66,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label Yes",
    "condition": 81,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D13",
            "outlook": "Overcast",
            "humidity": 75,
            "wind": "Weak",
            "play": "Yes"
          }
        ],
        "stat": [
          {
            "Yes": 1,
            "group": "Yes"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 67,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 68,
    "parentId": null,
    "name": null,
    "info": "Function Call 15 iteration",
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode",
    "stepId": null
  },
  {
    "id": 69,
    "parentId": 14,
    "name": "leaf",
    "category": "treeViz",
    "info": " pure split - leaf node created with label No",
    "condition": 71,
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "dataset": [
          {
            "day": "D14",
            "outlook": "Rain",
            "humidity": 80,
            "wind": "Strong",
            "play": "No"
          }
        ],
        "stat": [
          {
            "No": 1,
            "group": "No"
          }
        ],
        "entropy": 0
      }
    }
  },
  {
    "id": 70,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line10"
    ],
    "category": "pseudoCode",
    "stepId": null
  }
]