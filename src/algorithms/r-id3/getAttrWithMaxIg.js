// import {cloneDeep} from "lodash";
import {generateId, createStep} from "../id3/stepModel.js";
import {getValueStatisticsGivenAttr, entropyOfDataset, getTargetClasses, filterDatasetByAttrValue, round} from "../id3/index.js";
import {detectType, binarySplitEntropy} from './rid3Utils.js';
//import { cloneDeep } from 'lodash';

export function getMaxIgAttr(gdata, gattributes, targetAttributeName, targetClasses, parentId, stepslist, condition)
{
  let s_entropy = entropyOfDataset(targetClasses, gdata, targetAttributeName)
  let ig = []
  let maxIg = 0
  let maxIgAttr = ''
  let maxIgAttrStat=[]

  let splitMeasureStep = createStep()
  splitMeasureStep.id=generateId()
  splitMeasureStep.parentId=parentId
  splitMeasureStep.name='split-measure'
  splitMeasureStep.condition=condition
  splitMeasureStep.type='split-measure'
  splitMeasureStep.outcome='animate'
  splitMeasureStep.pseudo.push("line4")
  splitMeasureStep.info = " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain"

  // splitMeasureStep['uniqe_values_per_attrs']=uniqe_values_per_attrs
  splitMeasureStep.tree.data['entropy']=s_entropy
  let spstat = getValueStatisticsGivenAttr(gdata, targetAttributeName, targetClasses)
  spstat['group']='all'
  splitMeasureStep.tree.data.dataset= gdata

  splitMeasureStep.tree.data.stat.push(spstat)

  let newparentId =splitMeasureStep.id
  let gainList = []
  gattributes.forEach(attr => {
    let attrType = detectType(gdata, attr)
    console.log('attr + ' + attr + '  type: 0'+ attrType)
    if (attrType === 'numeric') {


      let splitValue = binarySplitEntropy(gdata, attr, targetClasses, targetAttributeName)
      let step = createStep()
      step.id = generateId()
      step.parentId = newparentId
      step.name = attr
      step.type = 'split-measure-step'
      step.outcome = 'animate'
      step.condition = condition
      step.tree.data.entropy = splitValue.gain
      step.tree.data['ig'] = splitValue.gain
      step.tree.data.dataset = gdata
      //mock it for now
      step.tree.data.stat = [
        {
          "No": 3,
          "Yes": 2,
          "group": "Sunny",
          "Entropy of Subset": 0.97
        },
        {
          "No": 0,
          "Yes": 4,
          "group": "Overcast",
          "Entropy of Subset": 0
        },
        {
          "No": 2,
          "Yes": 3,
          "group": "Rain",
          "Entropy of Subset": 0.97
        }
      ]
      step.pseudo.push("line4")
      step.info = "Calculating Entropy and Information Gain for Attr: " + attr
      stepslist.push(cloneDeeper(step))

      gainList.push({ name: attr, aig: splitValue.gain, attrType: attrType, splitPoint: splitValue.value, step: cloneDeeper(step) })

      //calculate entropy info given numeric attr
    }
    else if (attrType === 'categorical')
    {

        let numberOfEntries = gdata.length
        let avgEntropyInformation = 0

        let stat = []

        let uniqe_values_per_attr = getTargetClasses(gdata, attr)
        uniqe_values_per_attr.forEach(value => {
          let count = 0
          let subset = filterDatasetByAttrValue(gdata, attr, value)

          count = subset.length
          let entropyOfSubset = entropyOfDataset(targetClasses, subset, targetAttributeName)


          let substat = getValueStatisticsGivenAttr(subset, targetAttributeName, targetClasses)
          substat['group'] = value
          substat['Entropy of Subset'] = entropyOfSubset
          //substat['subset']=subset

          stat.push(substat)

          //step.tree.id3-sample-data-sets.stat=stat

          avgEntropyInformation += ((count / numberOfEntries) * entropyOfSubset)


        })


        ig[attr] = round((s_entropy - avgEntropyInformation), 3)
        //console.log(attr+ " " + ig[attr] )


        let step = createStep()
        step.id = generateId()
        step.parentId = newparentId
        step.name = attr
        step.type = 'split-measure-step'
        step.outcome = 'animate'
        step.condition = condition
        step.tree.data.entropy = avgEntropyInformation
        step.tree.data['ig'] = ig[attr]
        step.tree.data.dataset = gdata
        step.tree.data.stat = stat
        step.pseudo.push("line4")
        step.info = "Calculating Entropy and Information Gain for Attr: " + attr
        stepslist.push(cloneDeeper(step))
        //splitMeasureStep.children.push(step)


        gainList.push({ name: attr, aIg: round((s_entropy - avgEntropyInformation), 3), attrType: attrType, step: cloneDeeper(step) })





    }

  })
  gainList.sort((a, b) => b.aIg - a.aIg)

  console.log('gainList')
  console.log(gainList)
  let maxAtt = gainList[0]

  splitMeasureStep.tree.data['maxIgAtrr'] = maxAtt.attr
  splitMeasureStep.tree.data['maxIg']=maxAtt.aIg
  stepslist.push(splitMeasureStep)

  return maxAtt
}

