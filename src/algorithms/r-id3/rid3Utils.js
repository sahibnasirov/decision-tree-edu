import { entropyOfDataset, round } from '../id3/index.js';

export function detectType (data, attr){
  let type = 'categorical'
  data.forEach(row => {
    if (typeof row[attr] === 'number') {
      type = 'numeric'
    }
    else type= 'categorical'
  })
  return type
}

export function  binarySplitEntropy(data, attr, targetClasses, targetAttributeName) {
  let dataF = cloneDeeper(data)
  dataF.sort((a,b)=>a[attr]-b[attr])

  let s_entropy = entropyOfDataset(targetClasses, dataF, targetAttributeName)
  let numberOfEntries = dataF.length
  let listA = []
  for (let i = 0; i < dataF.length ; i++) {
    let avgEntropyInformation = 0
    let value = dataF[i][attr]
    let newSet1 = dataF.filter(a => {return a.temp <= value })
    let newSet2 = dataF.filter(a => {return a.temp > value })
    let entropy1 = entropyOfDataset(targetClasses,newSet1,targetAttributeName)
    let entropy2 = entropyOfDataset(targetClasses,newSet2,targetAttributeName)
    avgEntropyInformation= ((newSet1.length / numberOfEntries) * entropy1)+
      ((newSet2.length/numberOfEntries)*entropy2)

    let gain = round(s_entropy-avgEntropyInformation, 3 )
    listA.push({value:value, gain:gain})
  }
  console.log(listA)
  listA.sort((a,b)=>b.gain-a.gain)
  return listA[0]

}