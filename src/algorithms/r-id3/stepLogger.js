import { createPseudoStep, createStep, generateId } from '../id3/stepModel.js';
import { getValueStatisticsGivenAttr } from '../id3/index.js';

function log1 (stepslist) {
  let stepInit = createPseudoStep();
  stepInit.id = generateId();
  stepInit.pseudo.push('line0');
  stepInit.pseudo.push('line1');
  stepInit.info="Algorithm Starts"
  stepslist.push(stepInit);
}


function log2(stepslist, iteration){
  let stepInit = createPseudoStep();
  stepInit.id = generateId();
  stepInit.pseudo.push('line0');
  stepInit.pseudo.push('line1');
  stepInit.info="Function Call " + iteration  + " iteration"
  stepslist.push(stepInit);
}


function log3(stepslist, sdata, stat,targetClasses,condition, parentId){
  let step = createStep();
  step.id = generateId();
  step.parentId = parentId;
  step.name = 'leaf';
  step.condition = condition;
  step.type = 'stopCondition';
  step.outcome = 'persist';
  step.pseudo.push('line2');
  //rewrite
  stat['group'] = targetClasses[0];
  step.tree.data.dataset= sdata
  step.tree.data.stat.push(stat);
  step.info =
    ' pure split - leaf node created with label ' + targetClasses[0];
  stepslist.push(cloneDeeper(step));
}

function log4(stepslist, stat){
  let step = createStep();
  step.id = generateId();
  step.parentId = parentId;
  step.name = 'Leaf Most common Attr';
  step.condition = condition;
  step.type = 'stopCondition';
  step.outcome = 'persist';
  step.pseudo.push('line3');

  stat['group'] = Object.keys(stat).reduce((a, b) =>
    stat[a] > stat[b] ? a : b,
  );
  step.tree.data.dataset= sdata
  step.tree.data.stat.push(stat);
  step.info = 'Non-target-attrs are empty- Stopping Criteria - Quit';

  stepslist.push(cloneDeeper(step));
}
function log5(stepslist, parentId, condition){
  let stepmajor = createStep();
  stepmajor.id = generateId();
  stepmajor.parentId = parentId;
  stepmajor.name = 'Leaf Most common Attr';
  stepmajor.condition = condition;
  stepmajor.type = 'stopCondition';
  stepmajor.outcome = 'persist';
  stepmajor.pseudo.push('line8');
  let stat = getValueStatisticsGivenAttr(
    sdata,
    targetAttributeName,
    targetClasses,
  );
  stat['group'] = Object.keys(stat).reduce((a, b) =>
    stat[a] > stat[b] ? a : b,
  );
  stepmajor.tree.data.stat.push(stat);
  stepmajor.info = 'Non-target-attrs are empty- Quit';
  stepslist.push(stepmajor);
}

export {log1, log2, log3, log4}