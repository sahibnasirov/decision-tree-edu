import {isEmpty} from 'lodash'
import { getTargetClasses, getValueStatisticsGivenAttr, filterDatasetByAttrValue } from '../id3/index.js';
import { getMaxIgAttr } from './getAttrWithMaxIg.js';
import { generateId, createStep, createPseudoStep } from '../id3/stepModel.js';
import { log1, log4, log3, log2 } from './stepLogger.js';
import {cloneDeep} from 'lodash';

export function buildId3DecisionTreeRID3(sdata, nontargetattrs, targetAttributeName, parentId) {
  var stepslist = [];
  log1(stepslist); 
  let iteration = 0;
  console.log(cloneDeep(sdata))
  function rid3(sdata, nontargetattrs, targetAttributeName, parentId, condition) {
    console.log(cloneDeep(sdata))
    iteration++;
    log2(stepslist, iteration);
    let targetClasses = getTargetClasses(sdata, targetAttributeName);

    /*" (2) If tuples in  D are all of the same class, then \n" +
    "(3) return N as a leaf node labeled with the class;"*/
    if (targetClasses.length === 1) {
      let stat = getValueStatisticsGivenAttr(
        sdata,
        targetAttributeName,
        targetClasses,
      );

      log3(stepslist, sdata, stat, targetClasses, condition, parentId);
      return 0;
    }
    else if (nontargetattrs.length === 0) {
      /*"(4) If attribute_list is empty then \n"+
      "\n(5) return N as a leaf node labeled with the majority class in	D \n"*/
      let stat = getValueStatisticsGivenAttr(
        sdata,
        targetAttributeName,
        targetClasses,
      );

      log4(stepslist, stat, parentId, condition);

      return 0;
    }

    else {
      /*"(6) Otherwise Apply attribute_selection_method(D, attribute_list)to find	the „best“	splitting_criterion." */
      let step = getMaxIgAttr(cloneDeep(sdata), nontargetattrs, targetAttributeName, targetClasses, parentId, stepslist, condition);
      //let step = stepObj.step;

      let maxIgAttr = step.name;
      let persistNode = cloneDeeper(step.step);
      persistNode.id = generateId();
      persistNode.condition = condition;
      persistNode.outcome = '';
      persistNode.type = 'chosen-attr';
      persistNode.info = maxIgAttr + ' is selected with Maximum Information Gain ' + persistNode.tree.data.ig;
      persistNode.pseudo.push('line5');
      stepslist.push(persistNode);
      var newparentId = persistNode.id;

      console.error("step")
      console.log(step)
      /*"(8) If splitting_attribute is discrete-valued and\n" +
    "multiway splits allowed then\n" +
    "(9) attribute_list <- attribute_list – splitting_attribute;\t//\tremove splitting_attribute"
*/      let index = nontargetattrs.indexOf(maxIgAttr);
      if (index > -1) {
        nontargetattrs.splice(index, 1);
        let pseudo6 = createPseudoStep();
        pseudo6.id = generateId();
        pseudo6.info = 'removed seleted attribute ' + maxIgAttr + ' from the non target attr list';
        pseudo6.pseudo.push('line6');
        stepslist.push(pseudo6);
      }
      if(step.attrType === 'numeric'){
        console.log('numeric')
        console.log(sdata)
        let rightN = sdata.filter(el => {
          return el[step.name] > step.splitPoint
        })
        console.log(rightN)
        let leftN = sdata.filter(el => {
          return el[step.name] <= step.splitPoint
        })
        console.log(leftN)

        // leftN.forEach(el => delete el[step.name])
        // rightN.forEach(el => delete el[step.name])

        let rightNode = rid3(rightN, nontargetattrs, targetAttributeName, parentId, condition)
        let leftNode = rid3(leftN, nontargetattrs, targetAttributeName, parentId, condition)
        console.log(leftN)

      }
      //if numeric
      else {




        let uniqeValuesForMaxIgAttr = getTargetClasses(sdata, maxIgAttr);
        let filteredSubsets = [];

        for (let i = 0; i < uniqeValuesForMaxIgAttr.length; i++) {
          let filteredSubset = {};
          filteredSubset['condition'] = uniqeValuesForMaxIgAttr[i];
          filteredSubset['filteredData'] = filterDatasetByAttrValue(
            sdata,
            maxIgAttr,
            uniqeValuesForMaxIgAttr[i],
          );
          filteredSubsets.push(filteredSubset);
        }

        /*(10) for each outcome j of splitting_criterion\n" +
      "    //partition the tuples and grow subtrees for each partition\n" +
      "    (11) let Dj be the set of id3-sample-data-sets tuples in D satisfying outcome j; // (a partition)"
  */
        let pseudo7 = createPseudoStep();
        pseudo7.id = generateId();
        pseudo7.pseudo.push('line7');
        stepslist.push(pseudo7);

        filteredSubsets.forEach((filteredSubset) => {
          condition = filteredSubset.condition;
          filteredSubset.filteredData.forEach((el) => {
            delete el[maxIgAttr];
          });

          if (isEmpty(filteredSubset.filteredData)) {
            //attach leaf with majority class
            console.log('attach leaf with majority class');
            log5(stepslist, parentId, condition);

          } else {
            let pseudo9 = createPseudoStep();
            pseudo9.id = generateId();
            pseudo9.pseudo.push('line9');
            stepslist.push(pseudo9);
            rid3(
              filteredSubset.filteredData,
              nontargetattrs,
              targetAttributeName,
              newparentId,
              condition,
            );
          }
        });
      }
      let pseudo10 = createPseudoStep();
      pseudo10.id = generateId();
      pseudo10.pseudo.push('line10');
      stepslist.push(pseudo10);
      return 0;
    }
  }

  rid3(sdata, nontargetattrs, targetAttributeName, parentId, 'unknown');
  return stepslist;
}
