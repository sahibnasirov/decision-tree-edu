export var weatherNumeric = [
  {day:'D1',outlook:'Sunny', temp:85, humidity:85, wind: 'Weak',play:'No'},
  {day:'D2',outlook:'Sunny', temp:80, humidity:90, wind: 'Strong',play:'No'},
  {day:'D3',outlook:'Overcast', temp:83, humidity:78, wind: 'Weak',play:'Yes'},
  {day:'D4',outlook:'Rain', temp:70, humidity:96, wind: 'Weak',play:'Yes'},
  {day:'D5',outlook:'Rain', temp:68, humidity:80, wind: 'Weak',play:'Yes'},
  {day:'D6',outlook:'Rain', temp:65, humidity:70, wind: 'Strong',play:'No'},
  {day:'D7',outlook:'Overcast', temp:64, humidity:65, wind: 'Strong',play:'Yes'},
  {day:'D8',outlook:'Sunny', temp:72, humidity:95, wind: 'Weak',play:'No'},
  {day:'D9',outlook:'Sunny', temp:69, humidity:70, wind: 'Weak',play:'Yes'},
  {day:'D10',outlook:'Rain', temp:75, humidity:80, wind: 'Weak',play:'Yes'},
  {day:'D11',outlook:'Sunny', temp:75, humidity:70, wind: 'Strong',play:'Yes'},
  {day:'D12',outlook:'Overcast', temp:72, humidity:90, wind: 'Strong',play:'Yes'},
  {day:'D13',outlook:'Overcast', temp:81, humidity:75, wind: 'Weak',play:'Yes'},
  {day:'D14',outlook:'Rain', temp:71, humidity:80, wind: 'Strong',play:'No'}
];
