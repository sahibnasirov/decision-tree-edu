      import {weatherNumeric} from './c4.5/c4.5-sample-data-sets/weather-data-with-numeric.js';
      import { getValueStatisticsGivenAttr } from './id3';
      import { cloneDeep } from 'lodash';

      window.data = weatherNumeric
      console.log(weatherNumeric)

      var nontargetattrs = ['outlook', 'temp', 'humidity', 'wind'];
      var targetClasses = ["Yes", "No"]
      var targetAttributeName = "play";

      function log2(n) {
      // console.log(Math.log(n)/Math.log(2))
      return Math.log(n)/Math.log(2);

      }

      function round (number,  decimal) {
      let dec = Math.pow(10, decimal)
      return (Math.round((number + Number.EPSILON) * dec) / dec);
      }

      function entropyOfDataset(targetClasses, dataset, targetAttributeName) {
      let result = 0;
      targetClasses.forEach(targetClass => {
        //console.log(targetClass)
        let count = 0;
        dataset.forEach(row => {
          //console.log(row[targetAttributeName])
          if (row[targetAttributeName] === targetClass)
            count++;
        })
        let p = (count/dataset.length)
        if (p === 0){
          result+= 0
        }
        else {
          result+= -p*log2(p);
        }
      })
      //console.log((Math.round((result + Number.EPSILON) * 100) / 100))
      return round(result,2);
      }


      function uniqeValuesPerAttrs(data, attrs) {
        let result = {};
        attrs.forEach(attr => {
          let uniqueValues = [];
          data.forEach(row => {
            if (!uniqueValues.includes(row[attr], 0))
              uniqueValues.push(row[attr])
          })

          result[attr] = uniqueValues;
        })
        return result;
      }

      function uniqeValuesGivenAttr(data, attr) {
          let uniqueValues = [];
          data.forEach(row => {
            if (!uniqueValues.includes(row[attr], 0))
              uniqueValues.push(row[attr])
          })

        return uniqueValues;
      }

      function getTargetClasses(data, targetAttributeName) {
      let uniqueValues = [];
      data.forEach(row => {
        if (!uniqueValues.includes(row[targetAttributeName]))
          uniqueValues.push(row[targetAttributeName])
      })

      return uniqueValues;
      }

      function filterDatasetByAttrValue(data, attr, val) {
      return data.filter(function (el) {
        return el[attr] === val;
      });
      }


      function detectType (data, attr){
        let type = 'categorical'
        data.forEach(row => {
          if (typeof row[attr] === 'number') {
            type = 'numeric'
          }
          else type= 'categorical'
        })
        return type
      }

      function  binarySplitEntropy(data, attr) {
        let dataF =_.cloneDeep(data)
        dataF.sort((a,b)=>a[attr]-b[attr])

        let s_entropy = entropyOfDataset(targetClasses, dataF, targetAttributeName)
        let numberOfEntries = dataF.length
        let listA = []
        for (let i = 0; i < dataF.length ; i++) {
          let avgEntropyInformation = 0
          let value = dataF[i][attr]
          let newSet1 = dataF.filter(a => {return a.temp <= value })
          let newSet2 = dataF.filter(a => {return a.temp > value })
          let entropy1 = entropyOfDataset(targetClasses,newSet1,targetAttributeName)
          let entropy2 = entropyOfDataset(targetClasses,newSet2,targetAttributeName)
          avgEntropyInformation= ((newSet1.length / numberOfEntries) * entropy1)+
            ((newSet2.length/numberOfEntries)*entropy2)

          let gain = round(s_entropy-avgEntropyInformation, 3 )
          listA.push({value:value, gain:gain})
        }
        console.log(listA)
        listA.sort((a,b)=>b.gain-a.gain)
        return listA[0]

      }

      //step 1 calculate
      export function getMaxIgAttr(gdata, gattributes, targetAttributeName, targetClasses, parentId, stepslist, condition)
      {
        let s_entropy = entropyOfDataset(targetClasses, gdata, targetAttributeName)
        let ig = []
        let maxIg = 0
        let maxIgAttr = ''
        let maxIgAttrStat=[]

        let splitMeasureStep = createStep()
        splitMeasureStep.id=generateId()
        splitMeasureStep.parentId=parentId
        splitMeasureStep.name='split-measure'
        splitMeasureStep.condition=condition
        splitMeasureStep.type='split-measure'
        splitMeasureStep.outcome='animate'
        splitMeasureStep.pseudo.push("line4")
        splitMeasureStep.info = " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain"

        // splitMeasureStep['uniqe_values_per_attrs']=uniqe_values_per_attrs
        splitMeasureStep.tree.data['entropy']=s_entropy
        let spstat = getValueStatisticsGivenAttr(gdata, targetAttributeName, targetClasses)
        spstat['group']='all'
        splitMeasureStep.tree.data.dataset= gdata

        splitMeasureStep.tree.data.stat.push(spstat)

        let newparentId =splitMeasureStep.id

        attributes.forEach(attr => {
          let attrType = detectType(gdata, attr)

          if (attrType === 'numeric') {


            let splitValue = binarySplitEntropy(gdata, attr)

            gainList.push({ name: attr, aig: splitValue.gain, attrType: attrType, splitPoint: splitValue.value })

            //calculate entropy info given numeric attr
          }
          else
          {
            let uniqe_values_per_attrs = uniqeValuesPerAttrs(gdata, gattributes)

            for (let attr in uniqe_values_per_attrs) {
              let numberOfEntries = gdata.length
              let avgEntropyInformation = 0

              let stat = []
              uniqe_values_per_attrs[attr].forEach(value => {
                let count = 0
                let subset = filterDatasetByAttrValue(gdata, attr, value)

                count = subset.length
                let entropyOfSubset = entropyOfDataset(targetClasses, subset, targetAttributeName)


                let substat = getValueStatisticsGivenAttr(subset, targetAttributeName, targetClasses)
                substat['group'] = value
                substat['Entropy of Subset'] = entropyOfSubset
                //substat['subset']=subset

                stat.push(substat)

                //step.tree.id3-sample-data-sets.stat=stat

                avgEntropyInformation += ((count / numberOfEntries) * entropyOfSubset)


              })


              ig[attr] = round((s_entropy - avgEntropyInformation), 3)
              //console.log(attr+ " " + ig[attr] )


              let step = createStep()
              step.id = generateId()
              step.parentId = newparentId
              step.name = attr
              step.type = 'split-measure-step'
              step.outcome = 'animate'
              step.condition = condition
              step.tree.data.entropy = avgEntropyInformation
              step.tree.data['ig'] = ig[attr]
              step.tree.data.dataset = gdata
              step.tree.data.stat = stat
              step.pseudo.push("line4")
              step.info = "Calculating Entropy and Information Gain for Attr: " + attr
              stepslist.push(cloneDeep(step))
              //splitMeasureStep.children.push(step)


              gainList.push({ name: attr, aIg: round((s_entropy - avgEnropyInformation), 3), attrType: attrType })


            }
            console.log(gainList)
            gainList.sort((a, b) => b.aIg - a.aIg)
            return gainList[0];
        }})
     }


function buildId3DecisionTree(data, nontargetattrs, targetAttributeName)
    {


      if(nontargetattrs.length === 0){
        console.log('Non-target-attrs are empty')
        return null;
      }

      let targetClasses  = getTargetClasses(data, targetAttributeName)

      if (targetClasses.length === 1 ){
        console.log("end node! "+targetClasses[0]);
        return {type:"result", val: targetClasses[0], name: targetClasses[0]};
      }
      else{
        let maxIgAttr = getMaxIgAttr(data, nontargetattrs, targetAttributeName, targetClasses)
        let node = {}

        console.log(node)
        node.name = maxIgAttr.name
        node.type = "feature"

        if(maxIgAttr.attrType === 'numeric'){
          let index = nontargetattrs.indexOf(maxIgAttr.name)
          if (index > -1) {
            nontargetattrs.splice(index,1)
          }
         let rightN = data.filter(el => {
            el[maxIgAttr.attr] > maxIgAttr.splitPoint
          })
          let rightNode = buildId3DecisionTree(rightN, nontargetattrs, targetAttributeName)
          console.log(rightNode)
          rightN.forEach(el => delete el[maxIgAttr.name])
          let leftN = data.filter(el => {
            el[maxIgAttr.attr]<= maxIgAttr.splitPoint
            leftN.forEach(el => delete el[maxIgAttr.name])

          })

          let leftNode = buildId3DecisionTree(leftN, nontargetattrs, targetAttributeName)
          console.log(leftN)

        }
        else {
          let uniqeValuesForMaxIgAttr = getTargetClasses(data, maxIgAttr.name)

          node.vals = new Array(uniqeValuesForMaxIgAttr.length)
          console.log(node)

          for (let i = 0; i < uniqeValuesForMaxIgAttr.length ; i++) {

            let newData = filterDatasetByAttrValue(data, maxIgAttr, uniqeValuesForMaxIgAttr[i])
            newData.forEach(el => {
              delete el[maxIgAttr.name]
            })
            // remove maxIgAttr from nontargetattrs list
            let index = nontargetattrs.indexOf(maxIgAttr.name)
            if (index > -1) {
              nontargetattrs.splice(index,1)
            }
            let child_node = {name: uniqeValuesForMaxIgAttr[i],type: 'feature_value'}
            console.log("call buildId3DecisionTree funtion again")
            child_node.child = buildId3DecisionTree(newData, nontargetattrs, targetAttributeName)
            //console.log(child_node)
            node.vals[i]= child_node

        }

      }
      return node

      }
    }


      var tree = buildId3DecisionTree(weatherNumeric, nontargetattrs, targetAttributeName)
      console.log(tree)
      function predict(id3Model,sample){
      let root = id3Model;
      while(root.type !== "result"){
        var attr = root.name;
        var sampleVal = sample[attr];
        var childNode = ''
        for (let i = 0; i < root.vals.length; i++) {
          if (root.vals[i].name === sampleVal){
            childNode=root.vals[i].child
          }
        }
        let root = childNode;
      }
      return root.val;
      }

      var samples = [{outlook:'Overcast', temp:'Mild', humidity:'High', wind: 'Strong'},
      {outlook:'Rain', temp:'Mild', humidity:'High', wind: 'Strong', play: 'No'},
      {outlook:'Sunny', temp:'Cool', humidity:'Normal', wind: 'Weak', play: 'Yes'}]


      // var predict = predict(tree,samples[0])



      function proportionOfClassInEachGroup(){

      }




      function uniqe_values(){

      }


      function class_counter(traning_data){


      /* """Counts the number of each type of example in a dataset."""
        counts = {}  # a dictionary of label -> count.
        for row in rows:
            # in our dataset format, the label is always the last column
            label = row[-1]
            if label not in counts:
                counts[label] = 0
            counts[label] += 1
        return counts*/
      }
      function is_numeric(){

      }


      function partitionData(){

      }
