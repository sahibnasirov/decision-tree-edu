import {select, selectAll} from "d3"
import * as plines from "./pseudocode"
import {setWidthSvgResizeD} from '../../config-data/id3-config';

export function loadPseudoID3() {
var el =  document.getElementById("pseudocode");
    el.innerHTML = ""
    let sel = select("#pseudocode")
    sel = sel.selectAll("div").data(plines.lines)

    sel.enter().append("div")
        .attr("class", "flex-text")
        .attr("id", d=>"line"  + d.id)
        .html(d => d.line)
        // .html(d => "<br>" +d.line)

    // for (let i = 0; i < 11; i++) {
    //     let para = document.createElement("P");
    //     para.innerText = plines.lines[i].line;
    //     el.appendChild(para)
    //     para.id = "pseudo"+i
    //     let linebreak = document.createElement("br");
    //     el.appendChild(linebreak);
    //1
    // }

}

export function hidePseudo(el) {
    setWidthSvgResizeD(+400)
    document.getElementById(el).style.display="none"
    document.getElementById("showPseudo").style.display="block"

}
export function showPseudo(el) {
    setWidthSvgResizeD(-400)
    document.getElementById("showPseudo").style.display="none"
    document.getElementById(el).style.display="flex"

}
document.getElementById("showPseudo").addEventListener("click", show => {showPseudo("pseudocode-body")})

document.getElementById("hidePseudo").addEventListener("click", hide => hidePseudo("pseudocode-body"))


