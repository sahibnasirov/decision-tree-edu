function predict(id3Model,sample){
  root = id3Model;
  while(root.type !== "result"){
    var attr = root.name;
    var sampleVal = sample[attr];
    var childNode = ''
    for (let i = 0; i < root.children.length; i++) {
      if (root.children[i].condition === sampleVal){
        childNode=root.children[i]
      }
    }
    root = childNode;
  }
  return root.val;
}

var samples = [{outlook:'Overcast', temp:'Mild', humidity:'High', wind: 'Strong'},
  {outlook:'Rain', temp:'Mild', humidity:'High', wind: 'Strong', play: 'No'},
  {outlook:'Sunny', temp:'Cool', humidity:'Normal', wind: 'Weak', play: 'Yes'}]

var modelTree = buildId3DecisionTree(examples, nontargetattrs, targetAttributeName)
predict(modelTree,samples[0])