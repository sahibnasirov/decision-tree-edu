import { cloneDeep, isEmpty } from 'lodash';
import {
  getTargetClasses,
  getValueStatisticsGivenAttr,
  filterDatasetByAttrValue,
} from './util/utils';
import { getMaxIgAttr } from './getAttrWithMaxIg';
import { generateId, createStep, createPseudoStep } from './stepModel';

export function buildId3DecisionTree(
  sdata,
  nontargetattrs,
  targetAttributeName,
  parentId,
) {
  var stepslist = [];
  let stepInit = createPseudoStep();

  stepInit.id = generateId();
  stepInit.pseudo.push('line0');
  stepInit.pseudo.push('line1');
  stepInit.info="Algoritm Starts"
  stepslist.push(stepInit);

  let iteration = 0;
   function id3(
    sdata,
    nontargetattrs,
    targetAttributeName,
    parentId,
    condition,
  ) {
    iteration++
    let stepInit = createPseudoStep();
    stepInit.id = generateId();
    stepInit.pseudo.push('line0');
    stepInit.pseudo.push('line1');
    stepInit.info="Function Call " + iteration  + " iteration"
    stepslist.push(stepInit);

    let targetClasses = getTargetClasses(sdata, targetAttributeName);

    /*" (2) If tuples in  D are all of the same class, then \n" +
    "(3) return N as a leaf node labeled with the class;"*/
    if (targetClasses.length === 1) {
      let step = createStep();
      step.id = generateId();
      step.parentId = parentId;
      step.name = 'leaf';
      step.condition = condition;
      step.type = 'stopCondition';
      step.outcome = 'persist';
      step.pseudo.push('line2');
      //rewrite
      let stat = getValueStatisticsGivenAttr(
        sdata,
        targetAttributeName,
        targetClasses,
      );
      stat['group'] = targetClasses[0];
      step.tree.data.dataset= sdata
      step.tree.data.stat.push(stat);
      step.info =
        ' pure split - leaf node created with label ' + targetClasses[0];
      stepslist.push(cloneDeep(step));
      return 0;
    } else if (nontargetattrs.length === 0) {
      /*"(4) If attribute_list is empty then \n"+
"\n(5) return N as a leaf node labeled with the majority class in	D \n"*/
      let step = createStep();
      step.id = generateId();
      step.parentId = parentId;
      step.name = 'Leaf Most common Attr';
      step.condition = condition;
      step.type = 'stopCondition';
      step.outcome = 'persist';
      step.pseudo.push('line3');
      let stat = getValueStatisticsGivenAttr(
        sdata,
        targetAttributeName,
        targetClasses,
      );
      stat['group'] = Object.keys(stat).reduce((a, b) =>
        stat[a] > stat[b] ? a : b,
      );
      step.tree.data.dataset= sdata
      step.tree.data.stat.push(stat);
      step.info = 'Non-target-attrs are empty- Stopping Criteria - Quit';

      stepslist.push(cloneDeep(step));
      return 0;
    }
    //console.log(_.cloneDeep(nontargetattrs.length))
    else {
      /*"(6) Otherwise Apply attribute_selection_method(D, attribute_list)to find	the „best“	splitting_criterion." */
      let step = getMaxIgAttr(
        sdata,
        nontargetattrs,
        targetAttributeName,
        targetClasses,
        parentId,
        stepslist,
        condition,
      );
      let maxIgAttr = step.name;
      let persistNode = cloneDeep(step);
      persistNode.id = generateId();
      persistNode.condition = condition;
      persistNode.outcome = '';
      persistNode.type = 'chosen-attr';
      persistNode.info =
        maxIgAttr +
        ' is selected with Maximum Information Gain ' +
        persistNode.tree.data.ig;
      persistNode.pseudo.push('line5');

      stepslist.push(persistNode);
      let newparentId = persistNode.id;

      /*"(8) If splitting_attribute is discrete-valued and\n" +
    "multiway splits allowed then\n" +
    "(9) attribute_list <- attribute_list – splitting_attribute;\t//\tremove splitting_attribute"
*/

      let index = nontargetattrs.indexOf(maxIgAttr);
      if (index > -1) {
        nontargetattrs.splice(index, 1);
        let pseudo6 = createPseudoStep();
        pseudo6.id = generateId();
        pseudo6.info = "removed seleted attribute "  + maxIgAttr + " from the non target attr list"
        pseudo6.pseudo.push('line6');
        stepslist.push(pseudo6);
      }

      let uniqeValuesForMaxIgAttr = getTargetClasses(sdata, maxIgAttr);
      let filteredSubsets = [];

      for (let i = 0; i < uniqeValuesForMaxIgAttr.length; i++) {
        let filteredSubset = {};
        filteredSubset['condition'] = uniqeValuesForMaxIgAttr[i];
        filteredSubset['filteredData'] = filterDatasetByAttrValue(
          sdata,
          maxIgAttr,
          uniqeValuesForMaxIgAttr[i],
        );
        filteredSubsets.push(filteredSubset);
      }

      /*(10) for each outcome j of splitting_criterion\n" +
    "    //partition the tuples and grow subtrees for each partition\n" +
    "    (11) let Dj be the set of id3-sample-data-sets tuples in D satisfying outcome j; // (a partition)"
*/    let pseudo7 = createPseudoStep();
      pseudo7.id = generateId();
      pseudo7.pseudo.push('line7');
      stepslist.push(pseudo7);

      filteredSubsets.forEach((filteredSubset) => {
        condition=filteredSubset.condition
        filteredSubset.filteredData.forEach((el) => {
          delete el[maxIgAttr];
        });

        if (isEmpty(filteredSubset.filteredData)) {
          //attach leaf with majority class
          console.log('attach leaf with majority class');
          let stepmajor = createStep();
          stepmajor.id = generateId();
          stepmajor.parentId = parentId;
          stepmajor.name = 'Leaf Most common Attr';
          stepmajor.condition = condition;
          stepmajor.type = 'stopCondition';
          stepmajor.outcome = 'persist';
          stepmajor.pseudo.push('line8');
          let stat = getValueStatisticsGivenAttr(
            sdata,
            targetAttributeName,
            targetClasses,
          );
          stat['group'] = Object.keys(stat).reduce((a, b) =>
            stat[a] > stat[b] ? a : b,
          );
          stepmajor.tree.data.stat.push(stat);
          stepmajor.info = 'Non-target-attrs are empty- Quit';
          stepslist.push(stepmajor);
        }
        else {
          let pseudo9 = createPseudoStep();
          pseudo9.id = generateId();
          pseudo9.pseudo.push('line9');
          stepslist.push(pseudo9);
          id3(
            filteredSubset.filteredData,
            nontargetattrs,
            targetAttributeName,
            newparentId,
            condition,
          );
        }
      });
      let pseudo10 = createPseudoStep();
      pseudo10.id = generateId();
      pseudo10.pseudo.push('line10');
      stepslist.push(pseudo10);
      return 0;
    }
  }
   id3(sdata, nontargetattrs, targetAttributeName, parentId, 'unknown');
  return stepslist;
}
