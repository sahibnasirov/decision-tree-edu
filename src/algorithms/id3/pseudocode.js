
var line0 = "<pre>ID3 (D, Target_Attribute, attribute_list) </pre>"
var line1 = "<pre>(1) Create a node N </pre>"


var line2 = "<pre>(2) If tuples in  D are all of the same class, then" +
               "\n(3) \treturn N as a leaf node labeled with the class;</pre>"

var line3 = "<pre>(4) If attribute_list is empty then "+
               "\n(5) \t return N as a leaf node labeled"+
               "\n \t with the majority class in	D<pre>"

var line4 = "<pre>(6) Otherwise Apply"+
                "\n \tattribute_selection_method(D, attribute_list)"+
                     "\n \tto find	the 'best' splitting_criterion.</pre>"

var  line5 = "<pre>(7) Label node N with splitting_criterion;</pre>"


  var line6="<pre>(8) If splitting_attribute is discrete-valued and" +
      "\n\tmultiway splits allowed then " +
      "\n(9)\tattribute_list <- attribute_list – splitting_attribute;"+
       "\n\t\t//remove splitting_attribute</pre>"

var line7 = "<pre>     //partition the tuples and"+" grow subtrees for each partition" +
                 "\n(10) for each outcome j of splitting_criterion"+
                 "\n(11)     let <b>D</b><i><sub>j</sub></i> be the set of id3-sample-data-sets tuples in D"+
                 "\n         satisfying outcome j; // a partition"


var line8 ="<pre>(12) \t if <b>D</b><i><sub>j</sub></i> is empty then" +
                "\n(13) \t     attach a leaf labeled with" +
                "\n\t     the majority class in D to node N;</pre>"

var line9 = "<pre>(14)\t else attach the node returned by"+
                  "\n\t    Generate_decision_tree(<b>D</b><i><sub>j</sub></i>, attribute_list) to node N;</pre>"
               //  +"\n\n     end for-loop</pre>" // change here remove for-loop will not be higligted

var line10 = "<pre>     end for-loop"+
                "\n\n (15) return N</pre>"


var lines = []

for (let i = 0; i <=10; i++) {
  let id = i;
  let t = "line"+i
  let line = eval(t)
  let  lineobj = {id: id, line: line}
  lines.push(lineobj)

}
console.log(lines)
export {line0,line1,line2,line3,line4,line5,line6,line7,line8,line9, line10, lines}