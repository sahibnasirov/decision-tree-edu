import {select, selectAll} from "d3"
import  entropyViz from "./img/entropyViz.png"
import  formula from "./img/H(S).png"
import IGforAttrA from "./img/IG-wiki.png"
import { setWidthSvgResizeD } from '../../config-data/id3-config';
export function loadParameters() {

let p1 = "ID3 algorithm(Iterative Dichotomiser 3) is classification " +
  "algorithm for building decision tree designed for categorical data type"
let p2 = "ID3 uses Entropy Information and Information Gain to find best splitting attribute"

let p3 = 'Entropy <b>H(S)</b> is a measure for uncertainty in the dataset <b>S</b> .' +
  'Entropy Formula:'

let p4 = "Information Gain <b>IG(S,A)</b> shows how much uncertainty in dataset <b>S</b> will reduced " +
  "if we choose attribute <b>A</b> to split dataset <b>S</b>. IG Formula:"


var el =  document.getElementById("parameter");
  el.innerHTML = ""
  let  sel = select("#parameter")


  let selDiv = sel.append("div")
    .attr("style", "display:flex; flex-direction:column;")

    selDiv.append("br")

    selDiv.append("div").attr("class", "parametertext").html(p1)
    selDiv.append("div").attr("class", "parametertext").html(p2)
    selDiv.append("div").attr("class", "parametertext").html(p3)
    selDiv.append("div").style("margin-top", "10px").append("img").attr("src", formula)
    selDiv.append("div").attr("class", "parametertext").html(p4)
    selDiv.append("div").style("margin-top", "10px").append("img").attr("src", IGforAttrA)
    // selDiv.append("div").append("img").attr("src", entropyViz)

  let setHtml = "<p><strong>Steps in ID3 algorithm:</strong> </p>"+
    "<ol>\n" +
    "<li>Calculate entropy for dataset.</li>\n" +
    "<li>For each attribute/feature.<br>\n" +
    "2.1. Calculate Entropy for all its values.<br>\n" +
    "2.2. Calculate Average(weighted) Entropy for the feature.</li>\n" +
    "2.3. Calculate information gain for the feature.</li>\n" +
    "<li>Choose the feature with Maximum Information Gain.</li>\n" +
    "<li>Repeat it until Stop Condition meet (pure class or feature list empty).</li>\n" +
    "</ol>"
  let algoSteps = selDiv.append("div")
  algoSteps.html(setHtml)

  selDiv.append("div").html('<a href="https://en.wikipedia.org/wiki/ID3_algorithm">Reference</a>')

// var img1 = new Image();
  // img1.src= entropyViz
  // el.appendChild(img1)
}

export function hideParameter(el) {
  setWidthSvgResizeD(+300)
  document.getElementById(el).style.display="none"
  document.getElementById("showParameter").style.display="block"

}
export function showParameter(el) {
  setWidthSvgResizeD(-300)
  document.getElementById("showParameter").style.display="none"
  document.getElementById(el).style.display="block"

}
document.getElementById("showParameter").addEventListener("click", show => {showParameter("parameter")})

document.getElementById("hideParameter").addEventListener("click", hide => hideParameter("parameter"))



