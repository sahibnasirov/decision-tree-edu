import {cloneDeep} from "lodash";
import {generateId, createStep} from "./stepModel";
import {getValueStatisticsGivenAttr, entropyOfDataset, uniqeValuesPerAttrs, filterDatasetByAttrValue, round} from "./util/utils";
export function getMaxIgAttr(gdata, gattributes, targetAttributeName, targetClasses, parentId, stepslist, condition) {
  let s_entropy = entropyOfDataset(targetClasses, gdata, targetAttributeName)
  let uniqe_values_per_attrs = uniqeValuesPerAttrs(gdata, gattributes)
  let ig = []
  let maxIg = 0
  let maxIgAttr = ''
  let maxIgAttrStat=[]

  let splitMeasureStep = createStep()
  splitMeasureStep.id=generateId()
  splitMeasureStep.parentId=parentId
  splitMeasureStep.name='split-measure'
  splitMeasureStep.condition=condition
  splitMeasureStep.type='split-measure'
  splitMeasureStep.outcome='animate'
  splitMeasureStep.pseudo.push("line4")
  splitMeasureStep.info = " Appling Attribute selection to find best splitting Attribute with Maximum Information Gain"

  // splitMeasureStep['uniqe_values_per_attrs']=uniqe_values_per_attrs
  splitMeasureStep.tree.data['entropy']=s_entropy
  let spstat = getValueStatisticsGivenAttr(gdata, targetAttributeName, targetClasses)
  spstat['group']='all'
  splitMeasureStep.tree.data.dataset= gdata

  splitMeasureStep.tree.data.stat.push(spstat)

  let newparentId =splitMeasureStep.id



  for (let attr in uniqe_values_per_attrs) {
    let numberOfEntries = gdata.length
    let avgEntropyInformation = 0

    let stat = []
    uniqe_values_per_attrs[attr].forEach(value => {
      let count = 0
      let subset = filterDatasetByAttrValue(gdata, attr, value)

      count = subset.length
      let entropyOfSubset = entropyOfDataset(targetClasses, subset, targetAttributeName)


      let substat = getValueStatisticsGivenAttr(subset,targetAttributeName,targetClasses)
      substat['group'] = value
      substat['Entropy of Subset']=entropyOfSubset
      //substat['subset']=subset

      stat.push(substat)

      //step.tree.id3-sample-data-sets.stat=stat

      avgEntropyInformation += ((count / numberOfEntries) * entropyOfSubset)


    })


    ig[attr] = round((s_entropy - avgEntropyInformation), 3)
    //console.log(attr+ " " + ig[attr] )


    let step =createStep()
    step.id=generateId()
    step.parentId=newparentId
    step.name=attr
    step.type='split-measure-step'
    step.outcome='animate'
    step.condition=condition
    step.tree.data.entropy=avgEntropyInformation
    step.tree.data['ig']=ig[attr]
    step.tree.data.dataset= gdata
    step.tree.data.stat= stat
    step.pseudo.push("line4")
    step.info="Calculating Entropy and Information Gain for Attr: "+ attr
    stepslist.push(cloneDeep(step))

    splitMeasureStep.children.push(step)




    if (ig[attr] > maxIg) {
      maxIg = ig[attr]
      maxIgAttr = attr
      maxIgAttrStat=stat
    }

  }

  splitMeasureStep.tree.data['maxIgAtrr'] = maxIgAttr
  splitMeasureStep.tree.data['maxIg']=maxIg

  // later add sdtepslist after removing children
  let cloneSplitMeasureStep=cloneDeep(splitMeasureStep)

  try {
    delete cloneSplitMeasureStep.children
  }
  catch (e) {

  }
  stepslist.push(cloneSplitMeasureStep)

  let maxIGAtrr
  splitMeasureStep.children.forEach(child => {
    if(child.name === maxIgAttr){
      maxIGAtrr=child
    }
  })

  // maybe just return maxattr
  return maxIGAtrr
}

