export var  tempStepList = [
  {
    "id": 1,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 2,
    "parentId": 0,
    "name": "split-measure",
    "category": "treeViz",
    "condition": "unknown",
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 5,
            "Yes": 9,
            "group": "all"
          }
        ],
        "entropy": 0.94,
        "maxIgAtrr": "outlook",
        "maxIg": 0.247
      },
      "info": null
    }
  },
  {
    "id": 3,
    "parentId": 2,
    "name": "outlook",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "Yes": 2,
            "group": "Sunny",
            "entropyOfSubset": 0.97
          },
          {
            "No": 0,
            "Yes": 4,
            "group": "Overcast",
            "entropyOfSubset": 0
          },
          {
            "No": 2,
            "Yes": 3,
            "group": "Rain",
            "entropyOfSubset": 0.97
          }
        ],
        "entropy": 0.6928571428571428,
        "ig": 0.247
      },
      "info": null
    }
  },
  {
    "id": 4,
    "parentId": 2,
    "name": "temp",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 2,
            "Yes": 2,
            "group": "Hot",
            "entropyOfSubset": 1
          },
          {
            "No": 2,
            "Yes": 4,
            "group": "Mild",
            "entropyOfSubset": 0.92
          },
          {
            "No": 1,
            "Yes": 3,
            "group": "Cool",
            "entropyOfSubset": 0.81
          }
        ],
        "entropy": 0.9114285714285714,
        "ig": 0.029
      },
      "info": null
    }
  },
  {
    "id": 5,
    "parentId": 2,
    "name": "humidity",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 4,
            "Yes": 3,
            "group": "High",
            "entropyOfSubset": 0.99
          },
          {
            "No": 1,
            "Yes": 6,
            "group": "Normal",
            "entropyOfSubset": 0.59
          }
        ],
        "entropy": 0.79,
        "ig": 0.15
      },
      "info": null
    }
  },
  {
    "id": 6,
    "parentId": 2,
    "name": "wind",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 2,
            "Yes": 6,
            "group": "Weak",
            "entropyOfSubset": 0.81
          },
          {
            "No": 3,
            "Yes": 3,
            "group": "Strong",
            "entropyOfSubset": 1
          }
        ],
        "entropy": 0.8914285714285715,
        "ig": 0.049
      },
      "info": null
    }
  },
  {
    "id": 7,
    "parentId": 2,
    "name": "outlook",
    "category": "treeViz",
    "condition": "unknown",
    "type": "chosen-attr",
    "outcome": "persist",
    "pseudo": [
      "line5"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "Yes": 2,
            "group": "Sunny",
            "entropyOfSubset": 0.97
          },
          {
            "No": 0,
            "Yes": 4,
            "group": "Overcast",
            "entropyOfSubset": 0
          },
          {
            "No": 2,
            "Yes": 3,
            "group": "Rain",
            "entropyOfSubset": 0.97
          }
        ],
        "entropy": 0.6928571428571428,
        "ig": 0.247
      },
      "info": "outlook chosen with Maximum Information Gain 0.247"
    }
  },
  {
    "id": 8,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line6"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 9,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line7"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 10,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 11,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 12,
    "parentId": 7,
    "name": "split-measure",
    "category": "treeViz",
    "condition": "Sunny",
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "Yes": 2,
            "group": "all"
          }
        ],
        "entropy": 0.97,
        "maxIgAtrr": "humidity",
        "maxIg": 0.97
      },
      "info": null
    }
  },
  {
    "id": 13,
    "parentId": 12,
    "name": "temp",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 2,
            "Yes": 0,
            "group": "Hot",
            "entropyOfSubset": 0
          },
          {
            "No": 1,
            "Yes": 1,
            "group": "Mild",
            "entropyOfSubset": 1
          },
          {
            "No": 0,
            "Yes": 1,
            "group": "Cool",
            "entropyOfSubset": 0
          }
        ],
        "entropy": 0.4,
        "ig": 0.57
      },
      "info": null
    }
  },
  {
    "id": 14,
    "parentId": 12,
    "name": "humidity",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "Yes": 0,
            "group": "High",
            "entropyOfSubset": 0
          },
          {
            "No": 0,
            "Yes": 2,
            "group": "Normal",
            "entropyOfSubset": 0
          }
        ],
        "entropy": 0,
        "ig": 0.97
      },
      "info": null
    }
  },
  {
    "id": 15,
    "parentId": 12,
    "name": "wind",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 2,
            "Yes": 1,
            "group": "Weak",
            "entropyOfSubset": 0.92
          },
          {
            "No": 1,
            "Yes": 1,
            "group": "Strong",
            "entropyOfSubset": 1
          }
        ],
        "entropy": 0.9520000000000001,
        "ig": 0.018
      },
      "info": null
    }
  },
  {
    "id": 16,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 17,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 18,
    "parentId": 7,
    "name": "leaf",
    "category": "treeViz",
    "condition": "Overcast",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 4,
            "group": "Yes"
          }
        ],
        "entropy": null
      },
      "info": " pure split - leaf node Class Label Yes"
    }
  },
  {
    "id": 19,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 20,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 21,
    "parentId": 7,
    "name": "split-measure",
    "category": "treeViz",
    "condition": "Rain",
    "type": "split-measure",
    "outcome": "animate",
    "pseudo": [
      "line4"
    ],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 3,
            "No": 2,
            "group": "all"
          }
        ],
        "entropy": 0.97,
        "maxIgAtrr": "wind",
        "maxIg": 0.97
      },
      "info": null
    }
  },
  {
    "id": 22,
    "parentId": 21,
    "name": "temp",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 2,
            "No": 1,
            "group": "Mild",
            "entropyOfSubset": 0.92
          },
          {
            "Yes": 1,
            "No": 1,
            "group": "Cool",
            "entropyOfSubset": 1
          }
        ],
        "entropy": 0.9520000000000001,
        "ig": 0.018
      },
      "info": null
    }
  },
  {
    "id": 23,
    "parentId": 21,
    "name": "humidity",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 1,
            "No": 1,
            "group": "High",
            "entropyOfSubset": 1
          },
          {
            "Yes": 2,
            "No": 1,
            "group": "Normal",
            "entropyOfSubset": 0.92
          }
        ],
        "entropy": 0.9520000000000001,
        "ig": 0.018
      },
      "info": null
    }
  },
  {
    "id": 24,
    "parentId": 21,
    "name": "wind",
    "category": "treeViz",
    "condition": null,
    "type": "split-measure-step",
    "outcome": "animate",
    "pseudo": [],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 3,
            "No": 0,
            "group": "Weak",
            "entropyOfSubset": 0
          },
          {
            "Yes": 0,
            "No": 2,
            "group": "Strong",
            "entropyOfSubset": 0
          }
        ],
        "entropy": 0,
        "ig": 0.97
      },
      "info": null
    }
  },
  {
    "id": 25,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line10"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 26,
    "parentId": 12,
    "name": "humidity",
    "category": "treeViz",
    "condition": "Sunny",
    "type": "chosen-attr",
    "outcome": "persist",
    "pseudo": [
      "line5"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "Yes": 0,
            "group": "High",
            "entropyOfSubset": 0
          },
          {
            "No": 0,
            "Yes": 2,
            "group": "Normal",
            "entropyOfSubset": 0
          }
        ],
        "entropy": 0,
        "ig": 0.97
      },
      "info": "humidity chosen with Maximum Information Gain 0.97"
    }
  },
  {
    "id": 27,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line6"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 28,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line7"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 29,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 30,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 31,
    "parentId": 26,
    "name": "leaf",
    "category": "treeViz",
    "condition": "High",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 3,
            "group": "No"
          }
        ],
        "entropy": null
      },
      "info": " pure split - leaf node Class Label No"
    }
  },
  {
    "id": 32,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 33,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 34,
    "parentId": 26,
    "name": "leaf",
    "category": "treeViz",
    "condition": "Normal",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 2,
            "group": "Yes"
          }
        ],
        "entropy": null
      },
      "info": " pure split - leaf node Class Label Yes"
    }
  },
  {
    "id": 35,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line10"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 36,
    "parentId": 21,
    "name": "wind",
    "category": "treeViz",
    "condition": "Rain",
    "type": "chosen-attr",
    "outcome": "persist",
    "pseudo": [
      "line5"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 3,
            "No": 0,
            "group": "Weak",
            "entropyOfSubset": 0
          },
          {
            "Yes": 0,
            "No": 2,
            "group": "Strong",
            "entropyOfSubset": 0
          }
        ],
        "entropy": 0,
        "ig": 0.97
      },
      "info": "wind chosen with Maximum Information Gain 0.97"
    }
  },
  {
    "id": 37,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line6"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 38,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line7"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 39,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 40,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 41,
    "parentId": 36,
    "name": "leaf",
    "category": "treeViz",
    "condition": "Weak",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "Yes": 3,
            "group": "Yes"
          }
        ],
        "entropy": null
      },
      "info": " pure split - leaf node Class Label Yes"
    }
  },
  {
    "id": 42,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line9"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 43,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line0",
      "line1"
    ],
    "category": "pseudoCode"
  },
  {
    "id": 44,
    "parentId": 36,
    "name": "leaf",
    "category": "treeViz",
    "condition": "Strong",
    "type": "stopCondition",
    "outcome": "persist",
    "pseudo": [
      "line2"
    ],
    "children": [],
    "tree": {
      "data": {
        "stat": [
          {
            "No": 2,
            "group": "No"
          }
        ],
        "entropy": null
      },
      "info": " pure split - leaf node Class Label No"
    }
  },
  {
    "id": 45,
    "parentId": null,
    "name": null,
    "info": null,
    "pseudo": [
      "line10"
    ],
    "category": "pseudoCode"
  }
]