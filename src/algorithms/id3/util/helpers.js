import * as d3array from 'd3-array';
import * as d3color from 'd3-scale-chromatic'
import {targetClasses} from "../../../config-data/id3-commonVariables";

function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

export function sumStat( obj ) {
  var sum = 0;
  for( var el in obj ) {  
    if( obj.hasOwnProperty( el ) && typeof(obj[el]) === "number"  ) {
      sum +=  obj[el] ;
    }
  }
  return parseInt(sum);
}


export function filteredStat(nodeStat) {
  let filterStat=[]
  nodeStat.forEach(group => {
    let filteredgroup={}
    for(const key in group){
      if(targetClasses.includes(key)){
        filteredgroup[key] = group[key]
      }
    }
    filterStat.push(filteredgroup)
  })
  return filterStat
}
// sum stat rows
export function aggregate(array){
  let aggregated = []
  let obj ={}
  targetClasses.forEach(tclass=>{

    let sum= 0
    array.forEach(row => {
      if (Object.keys(row).includes(tclass)){
        sum+=row[tclass]
      }
    })
    obj[tclass]= sum
  })
  aggregated.push(obj)
  return aggregated
}






function groupMax(groupArray){
   return  d3array.max(filteredStat(groupArray), d=> sumStat(d))
}

function barChartYDomain(tree){
try{
  let nodelist = tree.children
  let domainList = nodelist.map(node => groupMax(node.data.tree.data.stat))
  return d3array.max(domainList)
}
catch(e){console.log(e)
return groupMax(tree.data.tree.data.stat)
  }
}
var colorObj = {}
function colorForTargetClasses() {
  let colorList = d3color.schemeSet2
   //[
//     "#2ca02c",
//     "#d62728",
//     "#1f77b4",
//     "#ff7f0e",
//     "#9467bd",
//     "#8c564b",
//     "#e377c2",
//     "#7f7f7f",
//     "#bcbd22",
//     "#17becf"
//   ]
  targetClasses.forEach( function (item, index) {
    colorObj[item] = colorList[index]
  });
  return colorObj

}
export {barChartYDomain, colorForTargetClasses, colorObj}