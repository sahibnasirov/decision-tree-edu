function log2(n) {
  // console.log(Math.log(n)/Math.log(2))
  return Math.log(n)/Math.log(2);


}
function round (number,  decimal) {
  let dec = Math.pow(10, decimal)
  return (Math.round((number + Number.EPSILON) * dec) / dec);


}
function entropyOfDataset(targetClasses, dataset, targetAttributeName) {
  let result = 0;
  targetClasses.forEach(targetClass => {
    //console.log(targetClass)
    let count = 0;
    dataset.forEach(row => {
      //console.log(row[targetAttributeName])
      if (row[targetAttributeName] === targetClass)
        count++;
    })
    let p = (count/dataset.length)
    if (p === 0){
      result+= 0
    }
    else {
      result+= -p*log2(p);
    }
  })
  //console.log((Math.round((result + Number.EPSILON) * 100) / 100))
  return round(result,2);
}
function uniqeValuesPerAttrs(data, attrs) {
  let result = {};
  attrs.forEach(attr => {
    let uniqueValues = [];
    data.forEach(row => {
      if (!uniqueValues.includes(row[attr], 0))
        uniqueValues.push(row[attr])
    })

    result[attr] = uniqueValues;
  })
  return result;
}
function getTargetClasses(data, targetAttributeName) {
  let uniqueValues = [];
  data.forEach(row => {
    if (!uniqueValues.includes(row[targetAttributeName]))
      uniqueValues.push(row[targetAttributeName])
  })

  return uniqueValues;
}
function filterDatasetByAttrValue(data, attr, val) {
  // console.log("input "+ " arrt " + attr + " value " +val )
  // console.log(id3-sample-data-sets)

  let filterd=null

  filterd= data.filter(function (el) {
    return el[attr] === val;
  })
  // console.log("output")
  // console.log(filterd)
  return filterd;
}
function getValueStatisticsGivenAttr(sampledata, tattrName, gTargetclasses  ) {
  if (sampledata[0][tattrName]===null || undefined){
    return 'given subset are empty or attr is not exist'
  }
  // let uniqeValues= getTargetClasses(sampledata, gattr)
  let valueStat={}
  //valueStat['group']=gattr
  gTargetclasses.forEach(tclass =>{
    let count = 0

    sampledata.forEach(el => {
      if(el[tattrName] === tclass){
        count+=1
      }
    })
    valueStat[tclass]=count
  })
  return valueStat

}

export {getTargetClasses, getValueStatisticsGivenAttr, filterDatasetByAttrValue, uniqeValuesPerAttrs, entropyOfDataset,log2, round}