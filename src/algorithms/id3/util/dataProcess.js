import * as d3 from "d3-hierarchy"
import {cloneDeep} from 'lodash';
import {margin, width, height} from "../../../config-data/id3-config";

var layout = d3.tree().size([width,height-190])
function cleanTreeData(treedata){
  let list =[]
  let tree = d3.hierarchy(treedata)
  tree.descendants().forEach(node =>{
      let step = _.cloneDeep(node.data)
      delete step.children
      list.push(step)
  })
  return list
  }

function repositionNodes(finalTreeNodes, ntree) {
  let tree =ntree
  tree.descendants().forEach(des => {
        finalTreeNodes.forEach(el => {
              if (des.id === el.id) {
                // console.log(
                //     des.x + "des.x +++  el.x" + el.x + " elId" + el.id3-sample-data-sets.id
                // )
                des.x = el.x
                des.y = el.y
              }
            }
        )
      }
  )
  return tree
}


function processTree(treedata){

  var cleanstepslist = cleanTreeData(treedata)

//order by node id
   cleanstepslist.sort((a, b) => parseInt(a.id) - parseInt(b.id));
   cleanstepslist[0].parentId = ""

  let n = cloneDeep(cleanstepslist.length)
  let treeslist = []

  let treeD = d3.stratify()(cleanstepslist)
  let finalTree = layout(treeD)
  let finalTreeNodes = finalTree.descendants().sort((a, b) => parseInt(a.id) - parseInt(b.id));

  let verticalDistance = finalTree.children[0].y -finalTree.y
  //console.log(verticalDistance)
  let currentStepList=[]
  for(let i = 0; i<n; i++){
    let newNode = cleanstepslist.shift()
    if((newNode.type !== "chosen-attr")) {
      currentStepList.push(newNode)

    }

    if(newNode.type === "chosen-attr"){
      let maxAttr = newNode.name
      currentStepList.forEach(el => {if(el.name === maxAttr){ el.outcome = "persist"}})

      let pid =  newNode.parentId

      currentStepList =  currentStepList.filter(el => { return el.parentId !== pid})

      let parentIndex = currentStepList.findIndex(el => el.id === pid)
     // console.log(currentStepList)
      if(currentStepList[parentIndex].parentId === ""){
        newNode.parentId = ""
      }
      else {

        let parentparentID = currentStepList[parentIndex].parentId
        newNode.parentId = parentparentID
    }

      currentStepList =  currentStepList.filter(el => { return el.id !== pid})
      currentStepList.push(newNode)
    }

    let tree = d3.stratify()(currentStepList)
    let treeHeight = tree.height+1
    let maxtreeHeight = 2 +1
    let curretgeight =  (height/maxtreeHeight)*treeHeight - 190
    tree  = d3.tree().size([width,curretgeight])(tree)

    console.log(cloneDeep(tree))



  tree.descendants().forEach(des => {
        try
          {
            let verticalDistance = des.children[0].y -des.y

            if(des.data.type === "split-measure")
              {
                des.children.forEach(ch =>
                  {
                    if(ch.data.outcome ==="animate")
                    ch.y = Math.round(ch.y - verticalDistance +60)
                  }
                )
              }
          }
        catch (e) {}

   })

    treeslist.push(tree)

  }
  //console.log(treeslist)
  return treeslist

}
function processDraft(treeData){

  var nodetree = layout(d3.hierarchy(treeData))

  var nodelist = cloneDeep(nodetree.descendants())
  var listremoved=[]
  for (let i=0; i<nodelist.length; i++){
    let step=cloneDeep(nodelist[i].data)
    step['x']= cloneDeep(nodelist[i].x)
    step['y']= cloneDeep(nodelist[i].y)
    step['depth']= cloneDeep(nodelist[i].depth)

    if (typeof step.children === 'undefined') {

    }
    else {
      delete step.children
      //delete step.id3-sample-data-sets
    }
    listremoved.push(step)

  }
  delete listremoved[0].parentId
  let clonedlist = cloneDeep(listremoved)
  let n = cloneDeep(listremoved.length)
  let treesofsteps = []
  //clonedlist.sort((a, b) => parseInt(a.id) - parseInt(b.id));

  for(let i = 0; i<n; i++){
    //let clist = _.cloneDeep(clonedlist)

    // clist.forEach(el =>
    //     {
    //       if (el.type ==="persist"){
    //         let nodePId =   node.id3-sample-data-sets.parentId
    //         // delete siblings assign node id of parent
    //
    //
    //       }
    //     }
    // )


    let tree = d3.stratify()(clonedlist)

    treesofsteps.push(tree)
    clonedlist.pop()

  }
  treesofsteps.reverse()
  return treesofsteps
}

export {processTree,layout}