export var credit =  [
  {
    RID: '1',
    age: 'youth',
    income: 'high',
    student: 'no',
    Credit_rating: 'fair',
    Buys_computer: 'no'
  },
    {
      RID: '2',
      age: 'youth',
      income: 'high',
      student: 'no',
      Credit_rating: 'excellent',
      Buys_computer: 'no'
    },
    {
      RID: '3',
      age: 'middle',
      income: 'high',
      student: 'no',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '4',
      age: 'senior',
      income: 'medium',
      student: 'no',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '5',
      age: 'senior',
      income: 'low',
      student: 'yes',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '6',
      age: 'senior',
      income: 'low',
      student: 'yes',
      Credit_rating: 'excellent',
      Buys_computer: 'no'
    },
    {
      RID: '7',
      age: 'middle',
      income: 'low',
      student: 'yes',
      Credit_rating: 'excellent',
      Buys_computer: 'yes'
    },
    {
      RID: '8',
      age: 'youth',
      income: 'medium',
      student: 'no',
      Credit_rating: 'fair',
      Buys_computer: 'no'
    },
    {
      RID: '9',
      age: 'youth',
      income: 'low',
      student: 'yes',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '10',
      age: 'senior',
      income: 'medium',
      student: 'yes',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '11',
      age: 'youth',
      income: 'medium',
      student: 'yes',
      Credit_rating: 'excellent',
      Buys_computer: 'yes'
    },
    {
      RID: '12',
      age: 'middle',
      income: 'medium',
      student: 'no',
      Credit_rating: 'excellent',
      Buys_computer: 'yes'
    },
    {
      RID: '13',
      age: 'middle',
      income: 'high',
      student: 'yes',
      Credit_rating: 'fair',
      Buys_computer: 'yes'
    },
    {
      RID: '14',
      age: 'senior',
      income: 'medium',
      student: 'no',
      Credit_rating: 'excellent',
      Buys_computer: 'no'
    }
  ]
export var creditI3R =  [
  {
    RID: '1',
    age: '20',
    income: 'high',
    student: 'no',
    Credit_rating: 'fair',
    Buys_computer: 'no'
  },
  {
    RID: '2',
    age: '19',
    income: 'high',
    student: 'no',
    Credit_rating: 'excellent',
    Buys_computer: 'no'
  },
  {
    RID: '3',
    age: '35',
    income: 'high',
    student: 'no',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '4',
    age: '65',
    income: 'medium',
    student: 'no',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '5',
    age: '67',
    income: 'low',
    student: 'yes',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '6',
    age: '80',
    income: 'low',
    student: 'yes',
    Credit_rating: 'excellent',
    Buys_computer: 'no'
  },
  {
    RID: '7',
    age: '40',
    income: 'low',
    student: 'yes',
    Credit_rating: 'excellent',
    Buys_computer: 'yes'
  },
  {
    RID: '8',
    age: '25',
    income: 'medium',
    student: 'no',
    Credit_rating: 'fair',
    Buys_computer: 'no'
  },
  {
    RID: '9',
    age: '18',
    income: 'low',
    student: 'yes',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '10',
    age: '87',
    income: 'medium',
    student: 'yes',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '11',
    age: '20',
    income: 'medium',
    student: 'yes',
    Credit_rating: 'excellent',
    Buys_computer: 'yes'
  },
  {
    RID: '12',
    age: '32',
    income: 'medium',
    student: 'no',
    Credit_rating: 'excellent',
    Buys_computer: 'yes'
  },
  {
    RID: '13',
    age: '29',
    income: 'high',
    student: 'yes',
    Credit_rating: 'fair',
    Buys_computer: 'yes'
  },
  {
    RID: '14',
    age: '90',
    income: 'medium',
    student: 'no',
    Credit_rating: 'excellent',
    Buys_computer: 'no'
  }
]
export var nonTarget = [
  "RID",
  "age",
  "income",
  "student",
  "Credit_rating",
  "Class Attr: Buys_computer"
]


export var targetVar = "Buys_computer"