
let counter =1
function  generateId(){
  let id = counter
  counter+=1
  return id
}
function  resetGenerateId(){
  counter =1

}

function createPseudoStep() {
  let step = {}
  step['id']= null
  step['parentId']= null
  step['name']=null
  step['info']=null
  step['pseudo']=[]
  step['category']='pseudoCode'
  step['stepId']= step.id


  return step
}

function createStep(){
  let step = {}
  step['id']= null
  step['parentId']= null
  step['name']=null
  step['category']='treeViz'
  step['info'] = null

  step['condition']=null

  step['type']=null
  step['outcome']=null
  step['pseudo']=[]

  step['children']=[]
  step['tree']={}
  step.tree['data']= {}
  step.tree.data['dataset']= []
  step.tree.data['stat']=[]
  step.tree.data['entropy']=0
  return step
}

export {resetGenerateId, generateId, createStep, createPseudoStep}
window.counter=counter