import Handsontable from "handsontable";
import 'handsontable/dist/handsontable.full.css';
import {select, selectAll} from "d3"
import { height, width } from '../config-data/id3-config';
let infoclickedbefore = false

 export function attrInfoOver(node , elid) {

   let tableData = [{name:"Attribute name", value: node.data.name},
     {name: "Entropy", value: parseFloat(node.data.tree.data.entropy).toFixed(3)},
     {name: "Information Gain", value: node.data.tree.data.ig}]

    console.log(tableData)
   let tabledivId = "attrInfo" + node.id
   select(elid)
     .append("g")
     .attr("id", "foreign" + tabledivId)
     .append("foreignObject")
     .style("transform", "translate(-110px, 0px)")
     .style("width", "240px")
     .style("height", "120px")
     .append("xhtml:div")
     .attr("id",tabledivId)




  var container = document.getElementById(tabledivId)
  var nodeTable = new Handsontable(container, {
    data: tableData,
    width: "240px",
    height: "120px",
    licenseKey: 'non-commercial-and-evaluation'

  });
  nodeTable.refreshDimensions();
}
export function attrInfoOut(node , elid) {
  if(!infoclickedbefore){
    infoclickedbefore=false
    let tabledivId = "foreignattrInfo" + node.id
    select(elid).selectAll("#"+tabledivId).remove()
  }


}
export function attrInfoClick(node , elid) {
  if(!infoclickedbefore)
  {
    infoclickedbefore=!infoclickedbefore
    attrInfoOver(node , elid)
  }
  else {infoclickedbefore=!infoclickedbefore
    attrInfoOut(node, elid)
  }

}
