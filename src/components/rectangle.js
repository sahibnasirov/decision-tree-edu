import {select} from "../libs/d3-multi";

export function rectangleSplitMeasure(splitnode, nodeId ){
 let g = select(nodeId)

 let leftNode = {
  x:splitnode.x-60,
  y:0
 }
 let rightNode = {
  x:splitnode.x+60,
  y:0
 }
if(splitnode.children !== undefined) {
 leftNode.x = splitnode.children[0].x
 leftNode.y = splitnode.children[0].y
 rightNode.x = splitnode.children[splitnode.children.length - 1].x
 rightNode.y = splitnode.children[splitnode.children.length - 1].y
}
  g.append("rect")
      .attrs(
        {
        x:leftNode.x-90,
        y: splitnode.y-20,
        rx:15,
        width: rightNode.x-leftNode.x+ 270,
        height: leftNode.y-splitnode.y +210,
        fill: 'transparent',
        stroke: 'aquamarine' })
      // .transition()
      // .duration(5000)
      // .attrs({ x: 460, y: 150, width: 40, height: 40, fill: 'blue' });
  g.append("text")
      .attr("class", "textRect")
      .attr("x", leftNode.x-50)
      .attr("y", splitnode.y)
      .attr("font-size","14px")
      .style("fill","black")
      .text("Split measure - selecting node with MaxIG")


}
export function rectangleNodeInfo(node, nodeId ){
   let infoTable = select(nodeId).append("g")

   let x = -60,
   y = -40,
   dheight = 30,
   width  =140

  function yposition(i){
      return y+ ((i)*dheight)
  }

  infoTable.append("rect")
      .attr("class","rect-info")
      .attr("x", x)
      .attr("y", y)
      .attr("height", 150)
      .attr("width",width)

//appand one rect
  var infoTableHeadersSelection = infoTable.selectAll(".info")
  // console.log(
  //     infoTableHeadersSelection
  // )

  var infoTableHeadersUpdate = infoTableHeadersSelection.data([{name:"Attribute name", value: node.data.name},
             {name: "Entropy", value: parseFloat(node.data.tree.data.entropy).toFixed(3)},
             {name: "Information Gain", value: node.data.tree.data.ig}])

  infoTableHeadersUpdate.enter()
      .append("rect")
      .attr("class", "rect-green")
      .attr("x", x)
      .attr("y", function (d,i) { return yposition(i)})
      .attr("height", dheight)
      .attr("width",width)

  infoTableHeadersUpdate.enter()
      .append("text")
      .text(text => text.name )
      .attr("x", x)
      .attr("y", function (d,i) { return yposition(i)+12})
      .attr("font-size", 12)

  infoTableHeadersUpdate.enter()
      .append("text")
      .text(text => text.value)
      .attr("x", x)
      .attr("y", function (d,i) { return yposition(i)+24})
      .attr("font-size", 12)




// move to original rectangle
  infoTable.attr("opacity", 0)
      .on('mouseover',function() {
        select(this)
            .transition()
            .duration(500)
            .attr('opacity',1)
      })
      .on('mouseout',function () {
        select(this)
            .transition()
            .duration(500)
            .attr('opacity',0)
      })


}