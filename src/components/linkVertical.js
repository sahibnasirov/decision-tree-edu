import {linkVertical} from 'd3'

function verticalSource(d) {
  return [d.source.x-10, d.source.y+30];
}

function verticalTarget(d) {
  return [d.target.x-10, d.target.y-30];
}
function verticalSourceClassA(d) {
  return [d.source.x+10, d.source.y+30];
}

function verticalTargetClassB(d) {
  return [d.target.x+10, d.target.y-30];
}

export function createPoints(sourceX,sourceY, targetX, targetY) {
  return {
    source: [sourceX, sourceY],
    target: [targetX, targetY]
  };

}

export var verticalLink = linkVertical()

var sankeyLinkVertical = linkVertical()
    .source(verticalSource)
    .target(verticalTarget);

var sankeyLinkVerticalClassA = linkVertical()
    .source(verticalSourceClassA)
    .target(verticalTargetClassB);
function addPath(elId, d) {
  let el = d3.select(elId).attr("path",sankeyLinkVertical)
}