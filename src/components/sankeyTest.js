import _ from 'lodash';
import {select} from 'd3'
import {targetClasses,numberOfEntry} from "../config-data/id3-commonVariables";
import {aggregate, filteredStat, colorObj} from "../algorithms/id3/util/helpers";
import {verticalLink, createPoints} from "./linkVertical";

let nodeCurrentLinkXPosition = {nodeId: null, position: {x:null, y: null }}

function generateNewLinkPosition(nodeId){
  let itemIndex = listnodeLinksPositions.findIndex(el => el.nodeId === nodeId)
  let newPosition = _.cloneDeep(listnodeLinksPositions[itemIndex].position.x) + 20
  listnodeLinksPositions[itemIndex].position.x  = newPosition
  return newPosition
}
function getNewPositionForSourceNode(nodeId) {
  let xp = generateNewLinkPosition(nodeId)
  let position={x:xp ,y:0}

  return position
}
function getNewPositionForTargetNode(nodeId) {
  let xp = generateNewLinkPosition(nodeId)
  let position={x:xp ,y:0}

  return position
}


function sankeyLink(el, link){
  link.source.id
  let g = select(el)

    var  nextSourcePositionX=0
    var nextTargetPositionX=0
//  console.log(link)
  try {


    targetClasses.forEach(tclass => {
      let sourceP = {x: link.source.x, y: link.source.y}
      let targetP = {x: link.target.x, y: link.target.y}
      let swidth = Math.round((((aggregate(filteredStat(link.target.data.tree.data.stat))[0][tclass]) * 104 / numberOfEntry)) * 100) / 100
      if (isNaN(swidth) || swidth === 0) {
        swidth = 0
      } else if (swidth > 0 && swidth < 1) {
        swidth = 1
      } else if (swidth >= 1) {
        swidth += 1
      }


      swidth > 0 && swidth < 1 ? swidth = 1 : (swidth = Math.log2(swidth) + 1)
      sourceP.x = (sourceP.x - swidth / 2) + nextSourcePositionX
      nextSourcePositionX += swidth

      targetP.x = (targetP.x - swidth / 2) + nextSourcePositionX
      nextTargetPositionX+= swidth
      g.append("path")
          .attr("class", "link")
          .attr("d", verticalLink(createPoints(sourceP.x - 30, link.source.y + 130, targetP.x - 20, link.target.y - 30)))
          .attr("stroke-width", swidth)
          .attr("fill", "none")
          .attr("stroke", colorObj[tclass])
      // .on('mouseout', function () {
      //   d3.select(this).attr('stroke-opacity', '1')
    })
  }
  catch (e) {

  }
}
function resetNextPosition() {

}


export {sankeyLink, resetNextPosition}