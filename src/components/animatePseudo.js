import {select, selectAll} from 'd3-selection'

  export function animatePseudo(pseudoList) {

    selectAll(".flex-text").classed("text-active", false)
    selectAll(".flex-text").classed("text-passive", true)
    pseudoList.forEach(
        pseudo => {
          select('#' + pseudo).classed("text-active", true)
        }
    )

  }

export function resetPseudo(pseudoList) {

  selectAll(".flex-text").classed("text-active", false)
  selectAll(".flex-text").classed("text-passive", true)

}