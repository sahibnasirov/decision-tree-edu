import {select} from 'd3-selection'
import {filteredStat, aggregate, colorObj} from "../algorithms/id3/util/helpers";

function numberForEachTargetClassValue(){
  let object = [{}]
  return object
}


var nodeWidth = 104
//var numberForEachTargetClassValue =0

function horizontalBar(el, node, numberOfEntry) {

  function getWidth(value){
    return Math.round((nodeWidth/numberOfEntry) * value)
  }

  let g = select(el).append("g")
  let stat = filteredStat(node.data.tree.data.stat)
  stat = aggregate(stat)

 // console.log(stat)
  g.append("rect")
      .attr("class","horizontal-bar")
      .attr("x", -20 )
      .attr("y", -36)
      .attr("width", 104)
      .attr("height", 14)
      .attr("fill", "transparent")
      .attr("stroke", "black")
      .attr("stroke-width", 2)

 let x =-20
  Object.keys((stat[0])).forEach(key => {
    g.append("rect")
        .attr("x", x )
        .attr("y", -36 )
        .attr("width", getWidth(stat[0][key]))
        .attr("height", 14)
        .attr("fill", colorObj[key] )
        .attr("stroke", "black")
        .attr("stroke-width", 1)

    x = x+getWidth(stat[0][key])
  })
}
export {horizontalBar}