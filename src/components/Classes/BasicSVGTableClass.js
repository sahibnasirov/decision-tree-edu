import {capitalizeFirstLetter} from '../../utils/utils'
import {select} from "d3";

export class BasicSVGTableClass {

  constructor(opts) {
    // load in arguments from config object
    this.data = opts.data;
    this.element = select('#' + opts.element);
    this.style = this.defaultStyle()
    this.columns = Object.keys(this.data[0])
    this.fontSize = 12
    this.cellWidthPerLetter = 6.2
    this.cellWidth=this.findCellWidth()
    this.tableWidth = this.findTableWidth()
    this.cellHeight=this.findCellHeight()
    this.tableHeight = this.findTableHeight()
    this.createSpaceBasedonData()
    this.draw();

  }
  setFontSize(size)
  {
    this.fontSize=size
    this.updateFontSize()
  }
  findCellHeight() {
    //TODO change to pixel actual height
    return Math.round(this.fontSize*1.8);
  }
  findTableHeight()
  {
    return Math.round((this.data.length +1.1 )*this.cellHeight)
  }
  findTableWidth()
  {
    return this.columns.length*this.cellWidth
  }
  findCellWidth() {
    let maxString = Math.max(...(this.columns.map(el => el.length)));
    let w = Math.round(maxString * this.cellWidthPerLetter);
    return w >= 44 ? w: 44

  }
  defaultStyle(){
    return {
      style:{
        backgroundColor: ''
      }

    }
  }



  updateFontSize(){
    this.cellWidth=this.findCellWidth()
    this.tableWidth = this.findTableWidth()
    this.createSpaceBasedonData()
    this.draw();
  }

  draw() {
    console.log(this.data)
    var columns = Object.keys(this.data[0])
    console.log(this.element)
    var tablediv = this.element

    var table = tablediv.append('table').attr('class', 'table-auto border-solid border-collapse')
    table.style("width", '100%')
      .style("height", '100%')
    var thead = table.append('thead')
    var	tbody = table.append('tbody');

    // append the header row
    thead.append('tr')
      // .style('font-size', this.fontSize + 'px')
      //.style('')
      .selectAll('th')
      .data(columns).enter()
      .append('th')
      .attr('class', 'border-collapse font-mono border-solid ')

      .text(function (column) { return capitalizeFirstLetter(column)})

    let rows = tbody.selectAll('tr')
      .data(this.data)
      .enter()
      .append('tr');

    let cells = rows.selectAll('td')
      .data(function (row) {
        return columns.map(function (column) {
          return {column: column, value: row[column]};
        });
      })
      .enter()
      .append('td')

    cells.attr('class', 'text-xs font-thin italic font-mono border-solid border-collapse hover:bg-sky-200 border-2 border-r-0')
    cells.classed('bg-red-50', function(d) {return d.column === 'play'})
    cells.text(function (d) { return d.value; })


  }

  createSpaceBasedonData() {
    this.element=this.element.append("foreignObject")
      .style("width", this.tableWidth +'px')
      .style("height", this.tableHeight +'px')
      .append("xhtml:div")
      .style("width", '100%')
      .style("height", '100%')
    // .style("margin", '1%')

  }

  addAxes() {

  }

  setSize(){

  }
  relocateTable(){}

  zoom()
  {}

  setEvent(){}
  remove(){}

  addLine() {
    const line = d3.line()
      .x(d => this.xScale(d[0]))
      .y(d => this.yScale(d[1]));

    this.plot.append('path')
      // use data stored in `this`
      .datum(this.data)
      .classed('line',true)
      .attr('d',line)
      // set stroke to specified color, or default to red
      .style('stroke', this.lineColor || 'red');
  }

  setColor(newColor) {
    this.plot.select('.line')
      .style('stroke', newColor);

    // store for use when redrawing
    this.lineColor = newColor;
  }

  setData(newData) {
    this.data = newData;

    // full redraw needed
    this.draw();
  }

  example() {
    // here, `this` is the chart
    const line = d3.svg.line()
      .x(function(d) {
        // but in here, `this` is the SVG line element
      })
  }
  example() {
    // here, `this` is the chart
    const line = d3.svg.line()
      .x(d => {
        // and in here, `this` is still the chart
      })
  }
  example() {
    // here, `this` is the chart
    const that = this;
    const line = d3.svg.line()
      .x(function(d) {
        // in here, `this` is the SVG line element
        // and `that` is the chart
      })
  }
}

// .style("transform", "translate(-80px, 140px)")

