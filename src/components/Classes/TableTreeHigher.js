import {select, selectAll,timeout, zoom, drag} from "d3"
import {width, height} from "../../config-data/id3-config";
import {TableTreeLower} from "./TableTreeLower";

export class TableTreeHigher {

    constructor() {
        this.resetSvg()
        this.treeRender = new TableTreeLower({svgEl: this.element})
        }


    addTreeState(treeState) {
        console.error(this.element)
        this.treeRender.setContainers(
            {   nodesVis: this.nodesVis,
                linksVis: this.linksVis,
                rectanglesVis: this.rectanglesVis,
            }
        )
        this.treeRender.render(treeState)
    }

    removeLinksRects()
    {
        this.linksVis.selectAll("g").remove()
        this.rectanglesVis.selectAll("g").remove()
    }

    resetSvg() {
        selectAll("svg").remove();
        this.element = select(".visualization").append('svg').attr('id', 'tableTree')
            // .attr("viewBox"," 0 0 " + width + " "+ height)
            // .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("width", width)
            .attr("height", height)
            .call(zoom().extent([[0, 0], [width, height]])
                .scaleExtent([0, 10])
                .on("zoom", this.zoomed))


        this.nodesAndLinks = this.element.append('g').attr("class", "perm")
            .attr("transform", "translate(" + 0 + "," + 50 + ")")
            .style
            ("background-color", " blue")

        this.nodesAndLinks.call(drag().on("drag",this.dragged).on("end", this.ended));

        this.nodesVis = this.nodesAndLinks.append('g').attr("class", "perm")
        this.rectanglesVis = this.nodesAndLinks.append('g').attr("class", "perm").style("pointer-events", "none")
        this.linksVis = this.nodesAndLinks.append('g').attr("class", "perm")

    }
    dragged(event, d) {
        console.log(event)
        console.log(d)
        this.nodesAndLinks.raise().attr("stroke", "black");
        this.nodesAndLinks.attr("transform", "translate(" + event.x+ "," + event.y + ")");

    }

    ended(event, d) {
        // console.log(event)
        // console.log(d)

        // nodesAndLinks.attr("transform", "translate(" + event.x -375 +"," + event.y + ")");
        this.nodesAndLinks.attr("stroke", null);
    }
    zoomed(event) {
        let transform = event.transform
        console.log(event)
        this.nodesAndLinks.attr("transform", event.transform)

        // .attr("width", widthSvg => {  let wid = transform.k*1000 + "px";
        //   console.log(wid + "<--width--k-->" +transform.k)
        //   return wid })
    }

}