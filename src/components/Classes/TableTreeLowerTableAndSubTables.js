import {rectangleSplitMeasure} from "../rectangle";
import {animatePseudo} from "../animatePseudo";
import {addStepInfo, addStepInfoOnSvg} from "../stepUserInfo";
import {sankeyLink} from "../customSankeyLink";
import {timeout} from "d3";
import {BasicSVGTableClass} from "./BasicSVGTableClass";

export class TableTreeLowerTableAndSubTables {
  constructor(opts) {
    this.svg = opts.svgEl
  }


  setContainers(opts){
    {
      this.nodesVis = opts.nodesVis
      // console.error(opts.nodesVis)
      this.rectanglesVis = opts.rectanglesVis
      this.linksVis = opts.linksVis
    }
  }
  render(treeState) {
    console.error(this.svg)
    console.log('this.svg.append(\'g\').attr(\'class\', \'yixar\')')
    var t = this.svg.transition()
      .duration(500)

    var root = treeState
    var nodesList = root.descendants()
    var linksList = root.links().filter(link => link.source.data.name !== "split-measure")
    var rectsList = root.descendants().filter(rect => {
      if (rect.data.name === "split-measure") {
        return rect
      }
    })

    var nodesVisUpdate = this.nodesVis.selectAll(".node").data(nodesList, d => d.id)
    var rectanglesVisUpdate = this.rectanglesVis.selectAll(".rectangle").data(rectsList, d=>d.id)
    var linksVisUpdate = this.linksVis.selectAll(".link-sankey").data(linksList/*, d => d.target.id*/)

    // console.log(cloneDeep(nodesVisUpdate))
    //var simulation = d3.forceSimulation(nodes);

    function rectEnter() {

      rectanglesVisUpdate.enter()
        .append("g")
        .attr("class", "rect")
        .attr("id", rect => "rect" + rect.data.id)
        .each(splitNode => rectangleSplitMeasure(splitNode, '#rect' + splitNode.data.id)
        )
      rectanglesVisUpdate.enter().append("circle").attr('r', 5)
      rectanglesVisUpdate.exit().remove()

    }
    rectEnter()






    nodesVisUpdate.transition(t)
      .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      })
      .attr("opacity", 0.8)

    //.on("end",nodesEnterFunc)

    function nodesEnterFunc() {

      let nodesEnter = nodesVisUpdate.enter()
        .append("g")
        .attr("class", "node")
        .attr("id", node => "node" + node.id)
        //.call(enter => enter.transition(t)
        .attr("transform", function (d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
        .attr("opacity", 1)


      nodesEnter.append("text")
        .attr("x", 45)
        .attr("y", -10)
        // .text(node => node.data.id + " - " + node.data.parentId)
        .each(vnode => {
          animatePseudo(vnode.data.pseudo)
          if (vnode.data.name !== "split-measure") {
            addStepInfoOnSvg("#node" + vnode.id, vnode)
            // horizontalBar('#node' + vnode.id, vnode, numberOfEntry);
            // stackedBarChart('#node' + vnode.id, vnode, barChartYDomain, colorObj, targetClasses, 14);
            new BasicSVGTableClass({data: vnode.data.tree.data.dataset, element:'node' + vnode.id})
          } else {
            addStepInfo(vnode.data.info)

          }
        })

      nodesEnter.enter()
        .append("rect")
        .attr("x", function (d) {
          return d.x
        })
        .attr("y", function (d) {
          return d.y
        })
        .attr("width", 8)
        .attr("height", 4)
        .attr("fill", "red")
    }

    let nodesExit = nodesVisUpdate.exit()
      .attr("opacity", 1)
      .call(exit => exit.transition(t).attr("opacity", 0.2)
        .remove())


    let linksEnter = linksVisUpdate.enter()
      .append("g")
      .attr("class", "link-sankey")
      .attr("id", function (d) {
        return 'linkId' + d.source.data.id + "to" + d.target.data.id
      })
      .each(link => {sankeyLink('#linkId' + link.source.data.id + "to" + link.target.data.id, link)})

    timeout(nodesEnterFunc, 1000)
  }

}