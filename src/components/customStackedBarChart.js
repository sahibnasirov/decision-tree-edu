import {select, scaleBand,scaleLinear, stack, axisBottom, axisLeft} from 'd3'
import { attrInfoOut, attrInfoOver, attrInfoClick } from './tableAttrInfo';
import {statTableClick, statTableOut, statTableOver, addstattable} from './tableStat';

let maxNumberOfArrtValues =3
export function stackedBarChart(elId, node, barChartYDomain, colorObj, subgroups, barWidth) {
  let stat = node.data.tree.data.stat
  var groups = stat.map(el => el.group)
  //var groups = d3.map(stat, function (d) { return (d.group)}).keys()

//  console.log(subgroups)
  const width = 64,
        height = 90


  let xRangeWidth = Math.round((width/maxNumberOfArrtValues)*groups.length)


  var x = scaleBand()
      .domain(groups)
      .range([0, xRangeWidth])
      .padding(0.1)

  var y = scaleLinear()
      .domain([0,barChartYDomain])
      .range([height, 0])

  // var color = d3.scaleOrdinal()
  //     .domain(Object.keys(colorObj))
  //     .range(Object.values(colorObj))

   var stackedData = stack()
      .keys(subgroups)
      (stat)

 // console.log(stackedData)




  let stackedBarChartDom= select(elId)

  let stackedBarChart = stackedBarChartDom
                          .append("g")
                          .attr("transform", "translate(0, 30)")
                          .style("pointer-events", "none")



  var xAxis = stackedBarChart.append("g")
      .attr("transform", "translate(0," + height+")")
      .attr("class", "x-axis")
      .call(axisBottom(x).tickSizeOuter(0.1))
      .selectAll("text")
      .attr("font-size","12px")
      .attr("fill","black")
      .attr("font-weight","bold")
      .attr("transform", "translate(0, -110) rotate(-45)")

  var yAxis = stackedBarChart.append("g")
      .attr("transform", "translate(" + 0 + ",0)")
      .attr("class", "y-axis")
      .call(axisLeft(y).ticks(null, "s"))



  var stackedBars =  stackedBarChart.append("g")
      .selectAll("g")
      .data(stackedData)

stackedBarChart.transition()
    .duration(200)

// stackedBarChart.transition()
//     .duration(10).delay(5000)

  stackedBars.enter()
      .append("g")
      .attr("fill", function(d) { return colorObj[d.key]; })
      .selectAll("rect")
      // enter a second time = loop subgroup per subgroup to add all rectangles
      .data(function(d) { return d; })
      .enter().append("rect")
      .attr("x", function(d) { return x(d.data.group); })
      .attr("y", function(d) { return y(d[1]); })
      .attr("height", function(d) { if(isNaN(d[0]) || isNaN(d[1])) {return 0 } else { return y(d[0]) - y(d[1]);}  })
      .attr("width",barWidth)









  var infoTableEnter = stackedBarChartDom.append("g").attr("id", "chartInfo"+elId)

  var statTable = infoTableEnter
      .append("rect")
      .attr("class","rect")
      .attr("x", -20)
      .attr("y", 10)
      .attr("height", 150)
      .attr("width",124)

  addstattable(node, elId)

    statTable
      .on('mouseover',call=>{statTableOver(node, elId)} )
      .on("mouseout", call=>{statTableOut(node, elId)})
      .on('click',call=>{statTableClick(node, elId)} )


  var chartName = infoTableEnter
      .append("rect")
      .attr("class","rect")
      .attr("x", -20)
      .attr("y", -20)
      .attr("height", 15)
      .attr("width",104)

  infoTableEnter
      .append("text")
      .attr("x", -15)
      .attr("y", -7)
      .text(node.data.name)

  chartName
    .on('mouseover',call=>{attrInfoOver(node, elId)} )
    .on("mouseout", call=>{attrInfoOut(node, elId)})
    .on('click',call=>{attrInfoClick(node, elId)} )


  // stackedBars.enter()
  //     .append("text")
  //     .text(function(d) {
  //       return d;
  //     })
  //     .attr("text-anchor", "middle")
  //     .attr("x", function(d, i) {
  //       return i * (w / dataset.length) + (w / dataset.length - barPadding) / 2;
  //     })
  //     .attr("y", function(d) {
  //       return h - (d * 4) + 14;
  //     })
  //     .attr("font-family", "sans-serif")
  //     .attr("font-size", "11px")
  //     .attr("fill", "white")


}



