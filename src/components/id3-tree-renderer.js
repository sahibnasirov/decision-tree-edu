import {select, selectAll,timeout, zoom, drag} from "d3"
import {horizontalBar} from "./horizontalBar";
import {stackedBarChart} from "./customStackedBarChart";
import {sankeyLink} from "./customSankeyLink";
import {rectangleSplitMeasure} from './rectangle'
import {addStepInfo, addStepInfoOnSvg} from "./stepUserInfo";
import { animatePseudo } from './animatePseudo';
import {targetClasses, barChartDomain as barChartYDomain} from "../config-data/id3-commonVariables";
import {colorObj} from "../algorithms/id3/util/helpers";
import {numberOfEntry} from "../config-data/id3-commonVariables";
import {margin, width, height} from "../config-data/id3-config"
//var transition = d3.transition()

export var svg
var nodesAndLinks
function dragged(event, d) {
  console.log(event)
  console.log(d)
  nodesAndLinks.raise().attr("stroke", "black");
  nodesAndLinks.attr("transform", "translate(" + event.x+ "," + event.y + ")");

}

function ended(event, d) {
  // console.log(event)
  // console.log(d)

  // nodesAndLinks.attr("transform", "translate(" + event.x -375 +"," + event.y + ")");
  nodesAndLinks.attr("stroke", null);
}
function zoomed(event) {
  let transform = event.transform
  console.log(event)
  nodesAndLinks.attr("transform", event.transform)

    // .attr("width", widthSvg => {  let wid = transform.k*1000 + "px";
    //   console.log(wid + "<--width--k-->" +transform.k)
    //   return wid })
}


export let nodesVis
export let rectanglesVis
export let linksVis


export function resetSvg() {
  selectAll("svg").remove();
  svg = select(".visualization").append('svg')
    // .attr("viewBox"," 0 0 " + width + " "+ height)
    // .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("width", width)
    .attr("height", height)


   svg.call(zoom().extent([[0, 0], [width, height]])
      .scaleExtent([0, 10])
      .on("zoom", zoomed))


  nodesAndLinks = svg.append('g').attr("class", "perm")
    .attr("transform", "translate(" + 0 + "," + 50 + ")")
  .style
  ("background-color", " blue")

  nodesAndLinks.call(drag().on("drag", dragged).on("end", ended));

  nodesVis = nodesAndLinks.append('g').attr("class", "perm")
  rectanglesVis = nodesAndLinks.append('g').attr("class", "perm").style("pointer-events", "none")
  linksVis = nodesAndLinks.append('g').attr("class", "perm")

}



function render(data) {
  const t = svg.transition()
      .duration(500)


  var nodesList
  var linksList
  var rectsList
  let root

  root = data
  nodesList = root.descendants()
  linksList = root.links().filter(link => link.source.data.name !== "split-measure")
  rectsList = root.descendants().filter(rect => {
    if (rect.data.name === "split-measure") {
      return rect
    }
  })

  window.noddeList = nodesList
  let nodesVisUpdate = nodesVis.selectAll(".node").data(nodesList, d => d.id)
  let rectanglesVisUpdate = rectanglesVis.selectAll(".rectangle").data(rectsList, d=>d.id)
  let linksVisUpdate = linksVis.selectAll(".link-sankey").data(linksList/*, d => d.target.id*/)

 // console.log(cloneDeep(nodesVisUpdate))
  //var simulation = d3.forceSimulation(nodes);

  function rectEnter() {

    rectanglesVisUpdate.enter()
      .append("g")
      .attr("class", "rect")
      .attr("id", rect => "rect" + rect.data.id)
      .each(splitNode => rectangleSplitMeasure(splitNode, '#rect' + splitNode.data.id)
      )
    rectanglesVisUpdate.enter().append("circle").attr('r', 5)
    rectanglesVisUpdate.exit().remove()

  }
  rectEnter()






  nodesVisUpdate.transition(t)
      .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      })
      .attr("opacity", 0.8)

     //.on("end",nodesEnterFunc)

  function nodesEnterFunc() {





    let nodesEnter = nodesVisUpdate.enter()
        .append("g")
        .attr("class", "node")
        .attr("id", node => "node" + node.id)
        //.call(enter => enter.transition(t)
        .attr("transform", function (d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
        .attr("opacity", 1)

    // )

    nodesEnter.append("text")
        .attr("x", 45)
        .attr("y", -10)
        // .text(node => node.data.id + " - " + node.data.parentId)
        .each(vnode => {
          animatePseudo(vnode.data.pseudo)
          if (vnode.data.name !== "split-measure") {
            addStepInfoOnSvg("#node" + vnode.id, vnode)
            horizontalBar('#node' + vnode.id, vnode, numberOfEntry);
            stackedBarChart('#node' + vnode.id, vnode, barChartYDomain, colorObj, targetClasses, 14);
          } else {
            addStepInfo(vnode.data.info)

          }
        })

    nodesEnter.enter()
        .append("rect")
        .attr("x", function (d) {
          return d.x
        })
        .attr("y", function (d) {
          return d.y
        })
        .attr("width", 8)
        .attr("height", 4)
        .attr("fill", "red")

    //nodesEnter.transition(t).delay(2000).attr("fill", "red")

// console.log(nodesEnter)

    //console.log(nodesUpdate)


    // console.log((new Date()).getMilliseconds());



}

  let nodesExit = nodesVisUpdate.exit()
      .attr("opacity", 1)
      .call(exit => exit.transition(t).attr("opacity", 0.2)
      .remove())





  //function linksEnterFunc() {
  let linksEnter = linksVisUpdate.enter()
      .append("g")
      .attr("class", "link-sankey")
      .attr("id", function (d) {
        return 'linkId' + d.source.data.id + "to" + d.target.data.id
      })
      .each(link => {sankeyLink('#linkId' + link.source.data.id + "to" + link.target.data.id, link)})

//}

  timeout(nodesEnterFunc, 1000)


  // let linksUpdate = linksVisList.merge(linksEnter)
  //     .transition()
  //     .duration(2000)
  // let linksExit = nodesVisList.exit().remove()

  // linksUpdate.each(link => sankeyLink('#linkId' + link.source.id3-sample-data-sets.id +"to" +link.target.id3-sample-data-sets.id, link))


  // rectanglesVisUpdate.exit().remove()

}
window.rende =render
window.svg =svg

export {render, margin, width, height}