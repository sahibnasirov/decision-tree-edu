import { TableTreeHigherTableAndSubTables} from "./Classes/TableTreeHigherTableAndSubTables";
import {setFinalNodes} from "./customSankeyLink";
let tableRenderer = new TableTreeHigherTableAndSubTables()

export function tableTreeVizualizer(treeState){
  setFinalNodes(treeState)
  tableRenderer.removeLinksRects()
  tableRenderer.addTreeState(treeState)
  console.log('table tree visualizer')
}
