import {select} from "d3"
function addStepInfo(info) {

  let div = select("#userMessage").html(null)

  div.append("div")
     .style("font-size", "20px")
     .style("text-align", "justify")
     .style(  "margin", "16px")
     .append("p").text(info)


  // g.append("text")
  //     .attr("x", -50 )
  //     .attr("y",  150)
  //     .text(message)
  //     .transition().duration(6000)
  //     .style("opacity", 0)


}


function addStepInfoOnSvg(el, vnode) {

  let stepInfoId = el + "stepInfo"

  select(el)
    .append("g")
    // .attr("id", stepInfoId)
    .append("foreignObject")
    .style("transform", "translate(-80px, 140px)")
    .style("width", "200px")
    .style("height", "120px")
    .append("xhtml:div")
    .attr("id",stepInfoId)
    .style("font-size", "17px")
    .style("text-align", "center")
    .style(  "margin", "16px")
    .append("xhtml:div")
    .style('border-radius', '10px').style('border-style', 'solid').style('border-color', 'aquamarine').style('border-width', '2px')
    .append("p").text(vnode.data.info)
    .transition()
    .duration(8000)
    .style('opacity',0)

}
export {addStepInfo, addStepInfoOnSvg}
