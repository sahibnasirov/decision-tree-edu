import { barChartYDomain as barChartYDomainFunc } from '../algorithms/id3/util/helpers';
import { setBarChartDomain } from '../config-data/id3-commonVariables';
import { setFinalNodes } from './customSankeyLink';
import { linksVis, rectanglesVis, render } from './id3-tree-renderer';

export function   barChartTreeVizualizer(treeState){
  /*let curretList = []
  if(step.id > nodeList[nodeList.length-1].id){
    nodeList.push(step)
    nodeList.sort((a, b) => parseInt(a.id) - parseInt(b.id));
    curretList= nodeList.slice(1,nodeList.length)
    console.log(curretList)
    let currentTree = builedTree(curretList)
    window.currentTree =  currentTree*/

  let barChartYDomain = barChartYDomainFunc(treeState)
  setBarChartDomain(barChartYDomain)
  setFinalNodes(treeState)

  linksVis.selectAll("g").remove()
  rectanglesVis.selectAll("g").remove()
  render(treeState)
  // }
  // else if(step.id < nodeList[nodeList.length-1].id){
  //   let index = nodeList.findIndex(item => item.id === step.id);
  //   curretList = nodeList.slice(1,index)
  //   nodeList = nodeList.slice(0,index)
  //
  //   console.log(curretList)
  //   let currentTree = builedTree(curretList)
  //   window.currentTree =  currentTree
  //
  //   let barChartYDomain = barChartYDomainFunc(currentTree)
  //   setBarChartDomain(barChartYDomain)
  //
  //   setFinalNodes(currentTree)
  //
  //   linksVis.selectAll("g").remove()
  //   rectanglesVis.selectAll("g").remove()
  //   render(currentTree)
  // }

}
