import { cloneDeep } from 'lodash';
import { select } from 'd3';
import { numberOfEntry, targetClasses } from '../config-data/id3-commonVariables';
import { aggregate, colorObj, filteredStat } from '../algorithms/id3/util/helpers';
import { createPoints, verticalLink } from './linkVertical';

let nodeCurrentLinkXPosition = {nodeId: null, position: {x:null, y: null }}
let listnodeLinksPositions = []
function  setFinalNodes(treedata){
  listnodeLinksPositions = []
  let finalnodes = cloneDeep(treedata)
  finalnodes.descendants().forEach(node => {
    // node['x'] = node.id3-sample-data-sets.x
    // node['y'] = node.id3-sample-data-sets.y
    // if(node.id3-sample-data-sets.type === 'split-measure-step'){
    //   node.y = node.y-80
    //   node.x = node.x-20
    // }
    let nodeCurrentLinkXPosition = {nodeId: node.data.id,
                                      position: {
                                        in: {
                                          x: Math.round(node.x),
                                          y: Math.round(node.y)
                                        },
                                        out: {
                                          x: Math.round(node.x),
                                          y: Math.round(node.y)
                                        }
                                      }

                                    }

    listnodeLinksPositions.push(nodeCurrentLinkXPosition)

  })

}


function getNewPositionForSourceNode(nodeId) {
  let itemIndex = listnodeLinksPositions.findIndex(el => el.nodeId === nodeId)
  let newPosition = cloneDeep(listnodeLinksPositions[itemIndex].position.out.x) + 12
  listnodeLinksPositions[itemIndex].position.out.x  = newPosition


  return  {x:newPosition ,y:0}

}
function getNewPositionForTargetNode(nodeId) {
  let itemIndex = listnodeLinksPositions.findIndex(el => el.nodeId === nodeId)
  let newPosition = cloneDeep(listnodeLinksPositions[itemIndex].position.in.x) + 12
  listnodeLinksPositions[itemIndex].position.in.x  = newPosition


  return { x: newPosition, y: 0 }
}

function sankeyLink(el, link){
  let links = select(el)
  let source = link.source
//  console.log(link)
  let linksUpdate = links.selectAll("path")
  let minswith = 2
  let totalw=1
  targetClasses.forEach(tclass => {
    let sourceP = getNewPositionForSourceNode(link.source.data.id)
    let targetP = getNewPositionForTargetNode(link.target.data.id)
    let ws  = aggregate(filteredStat(link.target.data.tree.data.stat))[0][tclass]
    console.log(ws)
    totalw+=ws
    let swidth = Math.round((ws*104)/numberOfEntry)
    console.log(swidth)
    if(isNaN(swidth) || swidth === 0){
      swidth=0
    }
    else if(swidth>0 && swidth<1){
      swidth=1
    }
    else if(swidth>=1){
      swidth+=1
    }



    swidth > 0 && swidth< 1 ? swidth =1: swidth = Math.round(swidth/2)
    sourceP.x = sourceP.x-swidth/2
    links.append("path")
        .attr("class", "link")
        .attr("d", path => {
          return verticalLink(createPoints(sourceP.x-5, link.source.y+130, targetP.x-20, link.target.y-35 ))
        })
        .attr("stroke-width", swidth)
        .attr("fill", "none")
        .attr("stroke", colorObj[tclass])
        .style('opacity', 0.5)



  })

  links.append("text")
    .attr("transform", "translate(" + (link.source.x+link.target.x-50)/2 +"," + (link.source.y+100+link.target.y)/2 +   ")")
    .text(link.target.data.condition)

  }

function resetNextPosition() {
  listnodeLinksPositions = []
}
export {setFinalNodes, sankeyLink, resetNextPosition}