import Handsontable from "handsontable";
import 'handsontable/dist/handsontable.full.css';
import {select, selectAll} from "d3"
let clickedbefore = false
let hovered = false


export function addstattable(node , elid) {
  let statData = node.data.tree.data.stat

  let colHeaders = Object.keys(statData[0])
  let tabledivId = "stattable" + node.id
  select(elid)
    .append("g")
    .attr("id", "foreign" + tabledivId)
    .append("foreignObject")
    .style("transform", "translate(-160px, 80px)")
    .style("width", "340px")
    .style("height", "120px")
    .append("xhtml:div")
    .attr("id", tabledivId)
    .style("backgraung-color", "white")
    .style('opacity', 0)


  var container = document.getElementById(tabledivId)
  var statTable = new Handsontable(container, {
    data: statData,
    colHeaders: colHeaders,
    width: "340px",
    height: "120px",
    licenseKey: 'non-commercial-and-evaluation'

  });
  statTable.refreshDimensions();


}



export function statTableOver(node , elid) {

    let tabledivId = "stattable" + node.id
    select("#"+ tabledivId)
      .style("backgraung-color", "white")
      .style('opacity', 1)


}
export function statTableOut(node , elid) {
if(!clickedbefore){
  let tabledivId = "stattable" + node.id
  select("#"+ tabledivId)
    // .style("backgraung-color", "white")
    .style('opacity', 0)
}


}

export function statTableClick(node , elid) {
  if(!clickedbefore)
  {
    clickedbefore=!clickedbefore
    statTableOver(node , elid)
  }
  else {clickedbefore=!clickedbefore
    statTableOut(node, elid)
  }

}