var targetClasses;
var numberOfEntry
var animationSpeed = 2000
var barChartDomain= 8

function setTargetClasses(x) {
  targetClasses=x
}

function setNumberofEntry(x) {
  numberOfEntry=x
}

function setBarChartDomain(x) {
  barChartDomain=x
}
export {targetClasses,numberOfEntry, setTargetClasses, setNumberofEntry, animationSpeed, barChartDomain, setBarChartDomain}