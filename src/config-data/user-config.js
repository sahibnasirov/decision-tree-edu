import { examples as ticTac, features as tcolheaders  } from '../algorithms/id3/id3-sample-data-sets/tic-tac-examples';
import { examples as weather, colHeaders as wcolHeaders } from '../algorithms/id3/id3-sample-data-sets/examples';
import {credit, nonTarget, targetVar} from '../algorithms/id3/id3-sample-data-sets/creditrating';
import {weatherNumeric} from '../algorithms/c4.5/c4.5-sample-data-sets/weather-data-with-numeric';
import {loadParameters as id3LoadParameters } from '../algorithms/id3/loadParameters';
import {loadPseudoID3} from '../algorithms/id3/id3-loadPseudo';


export function modelGenerate() {
  return {
    name: "",
    algo: "",
    dataSetName: "",
    id: "",
    model: null
  }
}

export var modelList = []


var config = {
  algo:null,
  dataset:null,
  splitMeasure:null,
  nodeVizTech: null
}

var dataProps = {
  chosenData : null,
  colHeaders : null
}



function getData(name) {
  switch (name) {
    case "weather":
      return weather;
    case "weatherNumeric":
      return weatherNumeric;
    case "tic-tac":
      return ticTac;
    case "credit":
      return credit;
  }
}
function getColHeaders(name) {
  switch (name) {
    case "weather":
      return wcolHeaders;
    case "weatherNumeric":
      return wcolHeaders;
    case "tic-tac":
      return tcolheaders;
    case "credit":
      return nonTarget;
  }
}
function setAlgo(chosenAlgo) {
  config.algo = chosenAlgo
  if(config.algo === 'id3'){
    id3LoadParameters()
    loadPseudoID3()
  }
  if(config.algo === 'c4.5'){
    // load c4.5 pseudo and parameters
  }

}

function setDataSet(datasetName) {
  config.dataset=datasetName
  dataProps.chosenData=getData(datasetName)
  dataProps.colHeaders = getColHeaders(datasetName)
}
function setCustomData(datasetName, dataset) {
  config.dataset=datasetName
  dataProps.chosenData=dataset
}

function setNodeVizTech(vizStyle){
  config.nodeVizTech = vizStyle
}

function setSplitMeasure(choosenMeasure) {
  config.splitMeasure = choosenMeasure
}
window.config = config
window.dataprops = dataProps

export {config,dataProps, setAlgo,setDataSet,setSplitMeasure, setNodeVizTech}
