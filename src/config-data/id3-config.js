import {select} from "d3"
export  var margin = {top: 100, right: 90, bottom: 50, left: 90}
export var width = 1000
export var height = 830
export var widthSvgResizeD = 0
export var heightSvgResizeD = 0

export function setWidthSvgResizeD(value) {
  widthSvgResizeD+=value
  select("svg").attr("width", width+widthSvgResizeD +"px")

}

export function setheightSvgResizeD(value) {
  select("svg").attr("height", value+"px")

}




export function resetId3Config() {
  margin = {top: 100, right: 90, bottom: 50, left: 90}
  width = 1000
  height = 830
  widthSvgResizeD = 0
  heightSvgResizeD = 0
}