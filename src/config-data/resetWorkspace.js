import {config} from "./user-config"
import {resetNavVariables } from '../controls/id3-navigation';
import {resetNodeList } from '../controls/id3-output-processor';
import {resetGenerateId} from '../algorithms/id3/stepModel';
import {resetId3Config} from './id3-config';

export function resetWorkspace() {
  resetId3Config()
  resetGenerateId()
  resetNavVariables()
  resetNodeList()

}